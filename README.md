# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Firm Media Builder 3.0

### Patch Notes ###
* Version 3.2.0 (P.C notes)
	* Revised Cutomizer "Sub Page Template" to work with full width widgets when using "10 centered"
	* Added Full Width Widgets on Sub Page Flexible fields
	* Improved utilization of CTA widget on Appearance > Widgets to work with Full width widgets
	* Plugins Updated
	* Various CCS and JS bug fixes
	* Database exported dbnewstart-ver3.2.0.sql
* Version 3.1.9 (P.C notes)
	* ADDED additional options to the "Sub Page Template" located at the Admin > Appearance > Customize (Sub Page / Archive)
		* 8/4 , 10 centered , 12 Contained , 12 expanded
	* ADDED a add a slider option on the Front Page Flexible fields under "Add new Row"
	* Various CCS and JS bug fixes
	* Database exported dbnewstart-ver3.1.9.sql
* Version 3.1.8 (P.C notes)
	* NEW version for Reviews Plugin
		* Will no longer use Custom Post Type (reviews) instead uses Page Template (Reviews). This will help to cut back SEO's time to no-index each individual review posts
		* This page template utilizes ACF Repeater with the Publish or Draft functions, Pretty much the same as the older plugin.
		* ACF fields should automatically activate with the use of the internal PHP function, Or can manually be imported using the json files.
	* Updated dependencies as follows:
		* Foundation from 6.4.3 to 6.5.1
		* GULP to ver 4.0
		* GULP SASS from 2.3.2 to 3.0		
		* what-input 5.1.3
		* Tested and working. Please do let me know if ever you're having problems running gulp.
	* NEW/UPDATED Off-canvas menu template
		* Added Social Links specifically for off-canvas navigation which includes a "title". the shortcode "offcanvasSocial" is located at theme-support.php line 316.
		* Implemented grid-y layout to keep the menu and call to action buttons responsive and always at 100% height. I also added "overflow:hidden" on the menu to give it a floating button illusion for the bottom CTA button
		* Renamed Customize option "Top Bar" to "Menu Bar". (Admin > Appearance > Customize)
		* Added Position and Transition functionality for TABLET and MOBILE sizes. We can now have two(2) variations of the nav for each media size. This is located at the "Customize" under "Menu Bar". Template is located at nav-offcanvas-mobile.php and JS at wp-foundation.js function name initOffCanvasSwitcher()
		* ADDED a "noScroll" class name to the BODY tag whenever the off-canvasnav opens. this will prevent scrolling while the off-canvas menu is OPEN. Also added a blur effect. CSS is located at global.scss under noScroll line 21-29
	* Updated the initCheckVideo() function, removed the deprecated videoonloadmeta and replaced it with a new jquery version of it.
	* Plugins Updated for Wordpress 5.0 (please update you local core install)
	* database exported dbnewstart-ver3.1.8.sql

* Version 3.1.7 (P.C notes)
	* Various CSS and Javascript code clean up (deleted unused/extra codes)
	* Admin Layout clean up on theme setting, Front page banner and subpages.
	* Plugins Updated
	* database exported dbnewstart-ver3.1.7.sql

* Version 3.1.6 (P.C notes)
	* Plugins Updated
	* Fixed Layout bugs on Microsoft EDGE and IE 11 and below.
	* Added Browser CSS detection for IE and EDGE, this includes their version as well. eg. ie-7 to ie-11 (see. wp-foundation.js function detectIE(); on line 593 )
	* Added Browser Compatibility SCSS folder under Assets, This contains exclusive SCSS files for all the major browsers and touch devices (Safari, firefox, IE , EDGE). i didn't include chrome cause we use it as a default development tool.
	* Added a local Font Awesome version for faster load speeds.	

* Version 3.1.5 (P.C notes)
	* Plugins Updated
	* Flickity Updated to 2.1.2
	* Added a popup message when the site is viewed on IE9 or earlier (see header.php line 58)

* Version 3.1.4 (P.C notes)
	* Various Update & Bug Fixes 
		* footer.php & frontpage-banner.php not using the new data-interchange php function.
		* JS: initBrowserExposure() not showing proper browser and OS class name on the HTML tag
		* JS: Cleaned up variable declarations by concatenating scripts
		* PHP: Removed unused php functions and tidy up variable declarations
		* Organized ACF fields positioning on the sub pages, theme options and front page.
		* Revised the script functionality on Gallery single pages so that it will not flicker when the image loads for the first time	
		* Renamed and Moved (parts/components) component-offcanvas.php to  (parts/navs) nav-offcanvas-mobile-menu.php to avoid confusion when altering the Off Canvas menu
		* Renamed the custom post type Procedures to Services.
		* Organized the Navigation items to show existing pages and layout examples
	* More initBrowserExposure() OS/Browser support including detection if the OS supports touch functionality. adds "touch" and "no-touch" class on the html tag	
	* Added a Fixed Parallax option for front-page flexible fields. we can now toggle between "perspective" and "fixed". please note that fixed positioning is not supported on mobile browsers as of now.
	* Added a Sub pages grid templates selector on the "Customize" option admin page:
		* Will dynamically alter and generate desired sub page template
		* located at Admin > Appearance > Customize > Sub Pages > Choose Sub Page Template
		* Three (3) options to choose from: Two Column (8/4 column), Full Width (10 columns centered) and Full width (12 columns)
	* Plugins Updated
	* Exported Database for the new Version labelled dbnewstart-ver3.1.4.sql

* Version 3.1.3 (J.W. notes)
	* Updated line 305 of /wp-content/themes/FMbuildr3.0/functions/theme-widgets.php php error corrected on assumed value of string `$align_text == center and $align_text == right ` changed to `$align_text == 'center' and $align_text == 'right'`
	
* Version 3.1.2 (P.C. notes)
	* Updated the Sub pages flexible fields; Added "Image slider" & "Content Slider" with flickity options and cell width as a different tab 
	* Posts now has endless scrolling pagination which uses wp localized script use JSON to make query requests and is handled by:
		* Use the class name "endless-scroll" on the main wrapper of your post list
		* JS: /assets/scripts/additional-scripts/load-more.js that targets the class name "endless-scroll"
		* STYLE: /styles/scss/global.scss ---- line 925
		* PHP : /functions/theme-support.php -- line 459 be_ajax_load_more()
		* PHP : /functions/enqueue-scripts.php --- line 49 be_load_more_js()				
	* Plugins Updated
	* Exported Database for the new Version labelled dbnewstart-ver3.1.2.sql

* Version 3.1.1(J.W. notes)
	* added conditional logic to new dataInterchangeImgs() function to prevent errors when no value is returned
	* removed second user for joshua from the database (no more  reference to webadmin@kurocloud in database) 

* Version 3.1.0 (P.C. notes) 
	* changed attachment image caption to 100% width on mobile
	* fixed the bug on the vp_get_thumb_url() php function that throws errors on the blog archive whenever there are no images added on a blog post.
	* wp-foundation.js update : we can use both jQuery and $ to create js scripts (inside document ready function).
	* added a new php function that replaces/changes how we pull images using data-interchange, now we can just use the php function : dataInterchangeImgs($theAcfImageVarible) and just assign the acf gallery variable. for application examples please refer to assets/functions/theme-support.php (line 75) for the php function and parts/components/components-banner.php line 37 and line 51
	* Plugins Updated
	* Exported Database for the new Version labelled dbnewstart-ver3.1.0

* Version 3.0.9 (P.C. notes) 
	* Added "Full Width Content (Text Only)" on sub-pages flexible fields. This can be used for embedding shortcodes, scripts and iframes without the worry of "Visual" messing up the code.
	* Added "Background Color" on the front page's flexi content, under New Row.
	* Added style to Gravity Form's Honeypot to display none	
	* Fixed blog template (home.php) php error under the php function vp_get_thumb_url().
	* Fixed Single Gallery template's php error having $endRowCount not defined.
	* Fixed Pop-up modal box's close button. it will no longer have a 100% width by default.
	* Cleaned PHP errors on subpages flexible field templates.
	* Cleaned SCSS code structures for front page flexible contents (frontpage-flexible.scss)
	* Cleaned and removed cluttered css on global.scss, footer.scss, widgets.scss , gravity-form.scss
	* Plugins Updated
	* Exported Database for the new Version labelled dbnewstart-ver3.0.9

* Version 3.0.8 (P.C. notes) 
	* Added the Plugin "Campaign Tracker for WordPress".
	* Fixed archive excerpt not showing "Read More" link whenever the post has an empty "excerpt" field.
	* Fixed the reviews plugin bug not showing Google Reviews Icon
	* Plugins Updated
	* Added "Two Column Widget" ACF relationship field for full/2col width templates.
	* Added "Contact Form Widget" ACF relationship field for full/2col width templates.
	* Added another type of Navigation template under Customizer > Top Bar , labelled "Off Canvas Tiles (mobile)". This will toggle the navigation with four(4) tiles when it hits 0px - 420px media size. It will use the template file nav-offcanvas-tiles.php, CSS will just relate to _navigation-bar.scss around line 153 to 214 (labels added for guide).
	* Exported Database for the new Version labelled dbnewstart-ver3.0.8	


* Version 3.0.7 (J.W. notes) 
	* disabled search engine indexing in read setting of wordpress settings
	* removed second user for Joshua
	* updated the in lines styles in component-frontpage-banner.php to reflect a mobile first structure
	* updated _frontpage-banner.scss to include sass variables and styles for offsetting the frontpage banner's h1 in relation to the .sticky-wrapper-navigation height. 
	
* Version 3.0.6 (P.C. notes)
	* Added TwentyTwenty.js for easy integration
	* Fixed Datepicker css bug when using gravity forms date picker
	* Added Base Background Color (color picker) on the ACF Text Widget
	* Updated Social tab on Theme Setting to support custom images and SVG for social or website fonts. ( SVG will have a default width of 30px )
	* Added a Warning message on the loader and navigation when a user's browser doesn't have Javascript enabled. (added a link for instructions)
	* Fixed the Mobile Expander wrapper's white gradient to not interfere with the expand and collapse button.	
	* Updated local url from http://fmbuildr.local to http://local.fmbuildr.com
	* Added new DB tagged dbnewstart-ver3.0.6


* Version 3.0.5 (P.C. notes)
	* Updated the template for loop-archive-excerpt.php. Post excerpts will now show a more cleaner Header, paddings and margins
	* Changed and disabled the function in initDebugin() that shows breakpoint console logs
	* Updated _banner.scss for a better uniform breakpoint coding. notice on the backend the acf field "Page Banner Fields" will have the text field "Banner Base Height", this will serve as a default for every breakpoint if the .banner-content class name's breakpoint sizes are not filled out.
	* Further fixed the flashing bug on the navigation using a CSS/JS that waits for the site to fully load Javascript using the class name .wait-js and associating it with a function inside initJsEssentials()
	* Deleted test.txt
	* Added new DB tagged dbnewstart-ver3.0.5

* Version 3.0.4 (P.C. notes)
	* using the class .stars will generate a row of 5 font awesome stars /n
	* Added the JS function initBrowserExposure() this will detect which browser and OS the site is being viewed and adds a class name on the HTML tag. this will help us on adding browser specific CSS when we're doing cross browser compatibilities.
	* fixed navigation bug by using the .no-js class, Also added a fallback functionality by adding a class name "wait-js" with 0 opacity then removing it when the javascript is ready.
	* Plugins Updated
	* Added Widget Templates for Reviews, Galleries and Request Consultation since it's a very common widget that we use.
	* Added _widget.scss on components folder
	* Added Links for Gallery, Reviews Plugin and Contact page on the starter navigation
	* Adjusted default Style Guide CSS for xmedium -> small
	* Fixed inconsistent variables on navigation-bar.scss
	* Added new DB tagged dbnewstart-ver3.0.4


* Version 3.0.3
	* cleaned up php syntax issues that created white screens when using php 7.+
	* updated setting.scss in include google fonts on site see comments 0. Fonts also commented out style_enqueue for google fonts so we can toggle this back on if needed.
	* replaced /plugins/kraken.io with wpsmushpro (better image compression)
	* added intuitive post-type plugin 
	* added expand-collapse layout button to mce fuctions
	
* Version 3.0.2
	* Fixed the bug that makes the mobile nav bar to have an extra space whenever that off-canvas menu open.
	* Added more scaffolding feature for front page's flexible field.
	* Updated Parallax Feature
	* Added Custom post type (procedure & gallery)
	* Installed the main plugins from the last builder
	
* Version 3.0.1
	* Implemented Flexible Fields on the Homepage (scaffolding)
	* Added Image/Video Banners Templates for the front page and sub pages
	* Started Parallax feature
	* Updated Settings.scss

* Version 3.0.0
	* Added Foundation XY grid
	* Added Flickity
	* Added Fancybox
	* Downloaded a fresh WP-JOINT
	* Added front-page.php
	* Added home.php


### How do I get set up? ###

* Summary of set up
* Configuration
	-- Run npm install when pulled locally
* Dependencies 
	-- Gulp
	-- Flickity
	-- Fancybox 3
* Database configuration
	-- Database is location: wp-content/db_bu/	
	-- With WP-CLI : Run -> wp db import db_bu/NAMEOFDATABASE.sql
	-- Use WP-CLI : Run -> wp search-replace "String that wants to be change" "string to replace it" 
* How to run tests
	-- Do : npm run browsersync

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Firm Media : webmaster@firm-media.com
