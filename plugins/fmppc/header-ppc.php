<html> 
  <head>
	  	<title><?php wp_title(''); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <?php wp_head(); ?>
	<?php 
	// 	get acf fields values for header
		$ppcHeader_img = get_field('ppc_logo','option');
		$ppcHeader_phonenumber = get_field('ppc_phone_number','option');
		$ppcHeader_address = get_field('ppc_address','option');
		$ppcHeader_bgcolor = get_field('ppc_header_background_color','option');
		$ppcHeader_txtcolor  = get_field('ppc_header_text_color','option');		
		$ppcFooter_bgcolor = get_field('footer_background_color','option');
		$ppcFooter_textcolor = get_field('footer_text_color','option');
		$ppcContact_bgcolor = get_field('background_color','option');
		$ppcContact_txtcolor = get_field('text_color','option');
		$ppcContact_linkcolor = get_field('link_color','option');
		$ppcButtoncolor = get_field('page_button_color','option');
		$ppcButtonhover = get_field('page_button_hover_color','option');
		$ppcButtontextclr = get_field('page_button_text_color','option');
		$ppcbgclr = get_field('page_background_color','option');
		$ppctextclr = get_field('page_font_color','option');
		$ppclinkclr = get_field('page_link_color','option');
		$ppclinkhvr = get_field('page_link_hover_color','option');
		$ppcHeadingclr = get_field('heading_color','option');
		$accordionclr = get_field('accordion_panel_color','option');
		$customcss = get_field('custom_css','option');
		$customjavascript = get_field('custom_javascript','option');

	?>
	<script>
		<?php echo $customjavascript; ?>
	</script>
	<style>
		
		body {
			color:<?=$ppctextclr;?>!important;
			background-color:<?=$ppcbgclr;?>!important;			
			}
		.pccMainWrapper h1, .pccMainWrapper h2, .pccMainWrapper h3 , .pccMainWrapper h4 , .pccMainWrapper h5 , .pccMainWrapper h6 {
			color:<?=$ppcHeadingclr?>!important;
			text-align: left!important ;
			}
		a:hover {
			color:<?=$ppclinkhvr;?>!important;
			}		
		a {
			color:<?=$ppclinkclr;?>!important;			
			}
		.ppc-btn, .pccMainWrapper .wpcf7 input[type="submit"] {
			background-color:<?=$ppcButtoncolor; ?>!important;
			color:<?=$ppcButtontextclr; ?>!important;
			}
	
		.ppc-btn:hover , .pccMainWrapper .wpcf7 input[type="submit"]:hover {
			background-color:<?=$ppcButtonhover; ?>!important;
			color:<?=$ppcButtontextclr; ?> !important ;
			}		
			
		.ppc-navbar-default{
				background-color:<?=$ppcHeader_bgcolor; ?>!important;
				color:<?=$ppcHeader_txtcolor; ?>!important;		
			}
		.ppc-navbar-default a {
				color:<?=$ppcHeader_txtcolor; ?>!important;
			}	
		.footerBar {
				background-color:<?=$ppcFooter_bgcolor; ?>!important;
				color:<?=$ppcFooter_textcolor; ?>;						
			}
		.pccMainWrapper .wpcf7 , .pccMainWrapper .wpcf7 form {	
				background-color: <?=$ppcContact_bgcolor; ?> !important;
				color:<?=$ppcContact_txtcolor; ?> !important;	
			}
		.pccMainWrapper .wpcf7 a {
				color:<?=$ppcContact_linkcolor; ?>!important;
			}
		.panel-default>.panel-heading {
		    color: <?=$ppctextclr; ?>!important;
		    background-color: <?=$accordionclr; ?>!important;
		    border:none !important;
		}	
		<?=$customcss; ?>				
	</style>    
  </head>
	<body <?php body_class( $class ); ?>>
	  <div class="navbar  navbar-fixed-top">
	    <div class="ppc-navbar-default">
	      	<div class="container navWrapper">
		      	<div class="row">	
					<div class="col-md-5 col-sm-5 col-xs-8">
						<div class="header_logo"> 
						<?php if($ppcHeader_img):
							$ppcHeader_logo_url = $ppcHeader_img['url'];	
							 ?>  					
							<img src="<?=$ppcHeader_logo_url?>"  alt="logo" width="<?=$ppcHeader_img['width']?>" height="<?=$ppcHeader_img['height']?>">     
						<?php endif; ?>	   			
			      		</div>	
					</div>
					<div class="col-md-7 col-sm-7 col-xs-4 ppc-xs-size">
						<div class="mdTopContact">
							<a href="tel:+1<?php echo preg_replace("/[^0-9,.]/", "", $ppcHeader_phonenumber) ?>"><i class="visible-xs fa fa-phone fa-2x phoneBtns"></i><span class="visible-xs phoneBtns">Call</span><span class="hidden-xs phoneMd"><?php echo $ppcHeader_phonenumber ?></span></a>
							<a href="#contactfrm" id="btncntnav" class="ppc-TopAddress"><i class="visible-xs fa fa-envelope-o fa-2x"></i><span class="visible-xs">Form</span></a><span class="hidden-xs rightText"> <?php echo $ppcHeader_address ?></span>
						</div><!--end mdTopContact-->
					</div><!-- end of md-col-7 -->
				</div><!-- end of row -->				
			</div><!-- end of container -->
	    </div><!--end of ppc-navbar-default-->
	  </div>
  <div class="container pccMainWrapper" style="min-height: 900px;">