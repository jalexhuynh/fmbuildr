<?php
/**
 * Plugin Name: FM PPC Template
 * Plugin URI: https://firm-media.com
 * Description: This plugin adds a ppc template with acf fields for drag and drop editing.
 * Version: 1.2.0
 * Author: Firm Media - Dev Team
 * Author URI: https://firm-media.com
 * License: GPL2
 */


if( function_exists( 'acf_add_options_sub_page' ) ) {
	acf_add_options_sub_page( array(
		'title'  => 'PPC Template Options',
		'parent' => 'themes.php',
	) );
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_560482a6590f9',
	'title' => 'PPC Single',
	'fields' => array (
		array (
			'key' => 'field_56048360dd7be',
			'label' => 'Content Areas',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_56048408dd7bf',
			'label' => 'Layouts',
			'name' => 'layouts',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Layout',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => '5604843b93349',
					'name' => 'two_column_gallery',
					'label' => 'Two Column Gallery',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_56048470dd7c0',
							'label' => 'Two Column Heading',
							'name' => 'two_column_heading',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_560484ebdd7c1',
							'label' => 'Two Column Gallery',
							'name' => 'two_column_gallery',
							'type' => 'gallery',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),
						array (
							'key' => 'field_560c2da0f3c64',
							'label' => 'Call to Action',
							'name' => 'call_to_action',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => 1,
							'layout' => 'row',
							'button_label' => 'Add CTA',
							'sub_fields' => array (
								array (
									'key' => 'field_560c2da0f3c65',
									'label' => 'CTA Link',
									'name' => 'cta_link',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array (
									'key' => 'field_560c2da0f3c66',
									'label' => 'CTA Text',
									'name' => 'cta_text',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array (
									'key' => 'field_560c60d5ffe9b',
									'label' => 'Alignment',
									'name' => 'alignment',
									'type' => 'radio',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'choices' => array (
										'left' => 'Left',
										'center' => 'Center',
										'right' => 'Right',
									),
									'other_choice' => 0,
									'save_other_choice' => 0,
									'default_value' => 'left',
									'layout' => 'vertical',
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '5604858add7c4',
					'name' => 'two_column_with_sidebar',
					'label' => 'Two Column with Sidebar',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_560af480c57f5',
							'label' => 'Two Column with Sidebar Heading',
							'name' => 'two_column_with_sidebar_heading',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_560b1575088c2',
							'label' => 'Left Content Area',
							'name' => 'content_area_container',
							'type' => 'flexible_content',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'button_label' => 'Add Content',
							'min' => '',
							'max' => '',
							'layouts' => array (
								array (
									'key' => '560b130fe5eef',
									'name' => 'content_field',
									'label' => 'Content Field',
									'display' => 'block',
									'sub_fields' => array (
										array (
											'key' => 'field_560b1575088c3',
											'label' => 'Content Field',
											'name' => 'content_txt_field',
											'type' => 'wysiwyg',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'default_value' => '',
											'tabs' => 'all',
											'toolbar' => 'full',
											'media_upload' => 1,
										),
									),
									'min' => '',
									'max' => '',
								),
								array (
									'key' => '560b1340b9872',
									'name' => 'accordion',
									'label' => 'Accordion',
									'display' => 'row',
									'sub_fields' => array (
										array (
											'key' => 'field_560b1575088c4',
											'label' => 'Accordion Title',
											'name' => 'accordion_title',
											'type' => 'text',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'default_value' => '',
											'placeholder' => '',
											'prepend' => '',
											'append' => '',
											'maxlength' => '',
											'readonly' => 0,
											'disabled' => 0,
										),
										array (
											'key' => 'field_560b1575088c5',
											'label' => 'Accordion Content',
											'name' => 'accordion_content',
											'type' => 'repeater',
											'instructions' => '',
											'required' => 1,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'min' => '',
											'max' => '',
											'layout' => 'table',
											'button_label' => 'Add List',
											'sub_fields' => array (
												array (
													'key' => 'field_560b1575088c6',
													'label' => 'Accordion List Title',
													'name' => 'accordion_list_title',
													'type' => 'text',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'prepend' => '',
													'append' => '',
													'maxlength' => '',
													'readonly' => 0,
													'disabled' => 0,
												),
												array (
													'key' => 'field_560b1575088c7',
													'label' => 'Accordion List Content',
													'name' => 'accordion_list_content',
													'type' => 'textarea',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'maxlength' => '',
													'rows' => '',
													'new_lines' => 'wpautop',
													'readonly' => 0,
													'disabled' => 0,
												),
											),
										),
									),
									'min' => '',
									'max' => '',
								),
								array (
									'key' => '561d56a441df7',
									'name' => 'gallery',
									'label' => 'Gallery',
									'display' => 'row',
									'sub_fields' => array (
										array (
											'key' => 'field_561d56b741df8',
											'label' => 'Gallery',
											'name' => 'gallery',
											'type' => 'gallery',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'min' => '',
											'max' => '',
											'preview_size' => 'thumbnail',
											'library' => 'all',
											'min_width' => '',
											'min_height' => '',
											'min_size' => '',
											'max_width' => '',
											'max_height' => '',
											'max_size' => '',
											'mime_types' => '',
										),
									),
									'min' => '',
									'max' => '',
								),
								array (
									'key' => '561d702839c06',
									'name' => 'cta_link_button',
									'label' => 'CTA link button',
									'display' => 'row',
									'sub_fields' => array (
										array (
											'key' => 'field_561d70be39c07',
											'label' => 'Call to Action',
											'name' => 'call_to_action',
											'type' => 'repeater',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'min' => '',
											'max' => 1,
											'layout' => 'row',
											'button_label' => 'Add CTA',
											'sub_fields' => array (
												array (
													'key' => 'field_561d70be39c08',
													'label' => 'CTA Link',
													'name' => 'cta_link',
													'type' => 'text',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'prepend' => '',
													'append' => '',
													'maxlength' => '',
													'readonly' => 0,
													'disabled' => 0,
												),
												array (
													'key' => 'field_561d70be39c09',
													'label' => 'CTA Text',
													'name' => 'cta_text',
													'type' => 'text',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'prepend' => '',
													'append' => '',
													'maxlength' => '',
													'readonly' => 0,
													'disabled' => 0,
												),
												array (
													'key' => 'field_561d70be39c0a',
													'label' => 'Alignment',
													'name' => 'alignment',
													'type' => 'radio',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'choices' => array (
														'left' => 'Left',
														'center' => 'Center',
														'right' => 'Right',
													),
													'other_choice' => 0,
													'save_other_choice' => 0,
													'default_value' => 'left',
													'layout' => 'vertical',
												),
											),
										),
									),
									'min' => '',
									'max' => '',
								),
								
							),
						),
						array (
							'key' => 'field_56048754dd7c6',
							'label' => 'Right Sidebar Content',
							'name' => 'sidebar_content',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '',
							'layout' => 'table',
							'button_label' => 'Add Widget',
							'sub_fields' => array (
								array (
									'key' => 'field_56048770dd7c7',
									'label' => 'Sidebar Widget',
									'name' => 'sidebar_widget',
									'type' => 'wysiwyg',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'tabs' => 'all',
									'toolbar' => 'full',
									'media_upload' => 1,
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '560487f7dd7c9',
					'name' => 'narrow_single_column',
					'label' => 'Single Column',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_560b1e5b443b1',
							'label' => 'Layout Width',
							'name' => 'layout_width',
							'type' => 'radio',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
								'full' => 'Full Width',
								'center' => 'Narrow Centered',
							),
							'other_choice' => 0,
							'save_other_choice' => 0,
							'default_value' => 'center',
							'layout' => 'horizontal',
						),
						array (
							'key' => 'field_560af4b7c57f6',
							'label' => 'Single Column Heading',
							'name' => 'narrow_single_column_heading',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_561d55be00a07',
							'label' => 'Single Col Sub Heading',
							'name' => 'single_col_sub_heading',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
						),

						array (
							'key' => 'field_560b0d9db7de8',
							'label' => 'Heading Alignment',
							'name' => 'heading_alignment',
							'type' => 'radio',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
								'center' => 'Center',
								'left' => 'Left',
								'right' => 'Right',
							),
							'other_choice' => 0,
							'save_other_choice' => 0,
							'default_value' => 'center',
							'layout' => 'horizontal',
						),
						array (
							'key' => 'field_560b0f988d2ac',
							'label' => 'Single Column Content Area',
							'name' => 'content_area_container',
							'type' => 'flexible_content',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'button_label' => 'Add Content',
							'min' => '',
							'max' => '',
							'layouts' => array (
								array (
									'key' => '560b130fe5eef',
									'name' => 'content_field',
									'label' => 'Content Field',
									'display' => 'block',
									'sub_fields' => array (
										array (
											'key' => 'field_560b132ab9871',
											'label' => 'Content Text Field',
											'name' => 'content_txt_field',
											'type' => 'wysiwyg',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'default_value' => '',
											'tabs' => 'all',
											'toolbar' => 'full',
											'media_upload' => 1,
										),
									),
									'min' => '',
									'max' => '',
								),
								array (
									'key' => '560b1340b9872',
									'name' => 'accordion',
									'label' => 'Accordion',
									'display' => 'row',
									'sub_fields' => array (
										array (
											'key' => 'field_560b1352b9873',
											'label' => 'Accordion Title',
											'name' => 'accordion_title',
											'type' => 'text',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'default_value' => '',
											'placeholder' => '',
											'prepend' => '',
											'append' => '',
											'maxlength' => '',
											'readonly' => 0,
											'disabled' => 0,
										),
										array (
											'key' => 'field_560b135db9874',
											'label' => 'Accordion Content',
											'name' => 'accordion_content',
											'type' => 'repeater',
											'instructions' => '',
											'required' => 1,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'min' => '',
											'max' => '',
											'layout' => 'table',
											'button_label' => 'Add List',
											'sub_fields' => array (
												array (
													'key' => 'field_560b13da91a2e',
													'label' => 'Accordion List Title',
													'name' => 'accordion_list_title',
													'type' => 'text',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'prepend' => '',
													'append' => '',
													'maxlength' => '',
													'readonly' => 0,
													'disabled' => 0,
												),
												array (
													'key' => 'field_560b13e791a2f',
													'label' => 'Accordion List Content',
													'name' => 'accordion_list_content',
													'type' => 'textarea',
													'instructions' => '',
													'required' => 0,
													'conditional_logic' => 0,
													'wrapper' => array (
														'width' => '',
														'class' => '',
														'id' => '',
													),
													'default_value' => '',
													'placeholder' => '',
													'maxlength' => '',
													'rows' => '',
													'new_lines' => 'wpautop',
													'readonly' => 0,
													'disabled' => 0,
												),
											),
										),
									),
									'min' => '',
									'max' => '',
								),
								array (
									'key' => '561d56a441df7',
									'name' => 'gallery',
									'label' => 'Gallery',
									'display' => 'row',
									'sub_fields' => array (
										array (
											'key' => 'field_561d56b741df8',
											'label' => 'Gallery',
											'name' => 'gallery',
											'type' => 'gallery',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array (
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'min' => '',
											'max' => '',
											'preview_size' => 'thumbnail',
											'library' => 'all',
											'min_width' => '',
											'min_height' => '',
											'min_size' => '',
											'max_width' => '',
											'max_height' => '',
											'max_size' => '',
											'mime_types' => '',
										),
									),
									'min' => '',
									'max' => '',
								),
							),
						),
						array (
							'key' => 'field_56048894dd7cc',
							'label' => 'Call to Action',
							'name' => 'call_to_action',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => 1,
							'layout' => 'row',
							'button_label' => 'Add CTA',
							'sub_fields' => array (
								array (
									'key' => 'field_560488aedd7cd',
									'label' => 'CTA Link',
									'name' => 'cta_link',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array (
									'key' => 'field_560488c2dd7ce',
									'label' => 'CTA Text',
									'name' => 'cta_text',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array (
									'key' => 'field_560c6148ffe9c',
									'label' => 'Alignment',
									'name' => 'alignment',
									'type' => 'radio',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'choices' => array (
										'left' => 'Left',
										'center' => 'Center',
										'right' => 'Right',
									),
									'other_choice' => 0,
									'save_other_choice' => 0,
									'default_value' => 'left',
									'layout' => 'vertical',
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '560b0af412e2b',
					'name' => 'add_horizontal_line',
					'label' => 'Add Horizontal Line',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_560b0c060e3d0',
							'label' => 'Color',
							'name' => 'color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#ccc',
						),
					),
					'min' => '',
					'max' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-ppc.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_560d7119cf0b5',
	'title' => 'PPC Theme',
	'fields' => array (
		array (
			'key' => 'field_560d725ab541e',
			'label' => 'Header',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_560d7344b541f',
			'label' => 'PPC logo',
			'name' => 'ppc_logo',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_560d73dcb5420',
			'label' => 'PPC Phone Number',
			'name' => 'ppc_phone_number',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_560d73f2b5421',
			'label' => 'PPC Address',
			'name' => 'ppc_address',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => 'wpautop',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_560d74a9b5422',
			'label' => 'PPC Header Background Color',
			'name' => 'ppc_header_background_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#cccccc',
		),
		array (
			'key' => 'field_560d74e3b5423',
			'label' => 'PPC Header Text Color',
			'name' => 'ppc_header_text_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000000',
		),
		array (
			'key' => 'field_560d75d4d848e',
			'label' => 'Color and Backgrounds',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_560d760cd8490',
			'label' => 'Page Background Color',
			'name' => 'page_background_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_560d7621d8491',
			'label' => 'Font Color',
			'name' => 'page_font_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000000',
		),
		array (
			'key' => 'field_560d7651d8492',
			'label' => 'Link Color',
			'name' => 'page_link_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#3199C5',
		),
		array (
			'key' => 'field_560d768ad8493',
			'label' => 'Link Hover Color',
			'name' => 'page_link_hover_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#45AEDA',
		),
		array (
			'key' => 'field_560d76c5d8494',
			'label' => 'Button Color',
			'name' => 'page_button_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#D9D9D9',
		),
		array (
			'key' => 'field_560d76fcd8495',
			'label' => 'Button Hover Color',
			'name' => 'page_button_hover_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#F1F1F1',
		),
		array (
			'key' => 'field_560d7748d8496',
			'label' => 'Button Text Color',
			'name' => 'page_button_text_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000000',
		),
		array (
			'key' => 'field_560d75e3d848f',
			'label' => 'Accordion Panel Color',
			'name' => 'accordion_panel_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#cccccc',
		),
		array (
			'key' => 'field_560d78e94e0d3',
			'label' => 'Heading Color',
			'name' => 'heading_color',
			'type' => 'color_picker',
			'instructions' => 'color for you h1 , h2 , h3 , etc',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000',
		),
		array (
			'key' => 'field_560d79154e0d4',
			'label' => 'Javascripts and CSS',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_560d79464e0d5',
			'label' => 'Load Bootstrap',
			'name' => 'bootstrap_loader',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'yes' => 'Yes',
				'no' => 'No',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => 'yes',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_560d797d4e0d6',
			'label' => 'Load Font Awesome',
			'name' => 'fontawesome_loader',
			'type' => 'radio',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'yes' => 'Yes',
				'no' => 'No',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_560d79a44e0d7',
			'label' => 'Custom Javascript (head)',
			'name' => 'custom_javascript',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_560d79c84e0d8',
			'label' => 'Custom CSS',
			'name' => 'custom_css',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '/* adjust padding of navigation and main container */
.container.pccMainWrapper{
	padding-top:100px;	
}',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_560d79fa4e0d9',
			'label' => 'Sidebar Contact Form',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_560d7a254e0da',
			'label' => 'Background Color',
			'name' => 'background_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#CACBCD',
		),
		array (
			'key' => 'field_560d7a524e0db',
			'label' => 'Text Color',
			'name' => 'text_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000000',
		),
		array (
			'key' => 'field_560d7a674e0dc',
			'label' => 'Link Color',
			'name' => 'link_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#369ECA',
		),
		array (
			'key' => 'field_560d7a944e0dd',
			'label' => 'Footer',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_560d7aa04e0de',
			'label' => 'Footer Content',
			'name' => 'footer_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
		),
		array (
			'key' => 'field_560d7ab94e0df',
			'label' => 'Footer Background Color',
			'name' => 'footer_background_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#cccccc',
		),
		array (
			'key' => 'field_560d7ad74e0e0',
			'label' => 'Text Color',
			'name' => 'footer_text_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#000000',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-ppc-template-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


if ( has_post_thumbnail() ) { 
    the_post_thumbnail( 'ppc_medium' ); 
}

add_image_size( 'ppc_medium', 250, 250, array( 'center', 'center' ) ); 


// ADD THIS FOR PPC BOOTSTRAP AND JQUERY 
function ppc_scripts($hook) {	
	if (is_page_template('page-ppc.php')) {
		global $post;
    $bootstrap = get_field('bootstrap_loader','option', $post->ID);
	    if( $bootstrap == 'yes' ) {
			wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' );	
			wp_enqueue_script( 'boostrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array('jquery'), '', true );
		}
	$fontawesome = get_field('fontawesome_loader', 'option', $post->ID);
	    if( $fontawesome == 'yes' ) {
		  wp_enqueue_style( 'font-awesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', array() );
	    }  
	       	wp_enqueue_style( 'ppc-theme', plugin_dir_url( __FILE__ ) . 'ppc-style/style.css', array() );
			wp_enqueue_script('custom-js', plugin_dir_url( __FILE__ ) . 'ppc-js/script.js', array( 'jquery' ),'', true );	 

	}
}
add_action( 'wp_enqueue_scripts', 'ppc_scripts' );


class PageTemplater {

	/**
	 * A reference to an instance of this class.
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Returns an instance of this class. 
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new PageTemplater();
		} 

		return self::$instance;

	} 

	/**
	 * Initializes the plugin by setting filters and administration functions.
	 */
	private function __construct() {

		$this->templates = array();


		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}

		// Add a filter to the save post to inject out template into the page cache
		add_filter(
			'wp_insert_post_data', 
			array( $this, 'register_project_templates' ) 
		);


		// Add a filter to the template include to determine if the page has our 
		// template assigned and return it's path
		add_filter(
			'template_include', 
			array( $this, 'view_project_template') 
		);


		// Add your templates to this array.
		$this->templates = array(
			'page-ppc.php' => 'PPC Template',
		);
			
	} 

	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
	 */
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. 
		// If it doesn't exist, or it's empty prepare an array
		$templates = wp_get_theme()->get_page_templates();
		if ( empty( $templates ) ) {
			$templates = array();
		} 

		// New cache, therefore remove the old one
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	} 

	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		
		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta( 
			$post->ID, '_wp_page_template', true 
		)] ) ) {
			return $template;
		} 

		$file = plugin_dir_path( __FILE__ ). get_post_meta( 
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

} 
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );


function SearchFilter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
      $query->is_search = false;
      $query->is_home = true;
   }
   return $query;
}
add_filter('pre_get_posts','SearchFilter');


function exclude_page_templates_from_search($query) {

    global $wp_the_query;

    if ( ($wp_the_query === $query) && (is_search()) && ( ! is_admin()) ) {

        $args = array_merge($wp_the_query->query, array(
        'meta_query' => array(
            array(
                'key' => '_wp_page_template',
                'value' => 'page-ppc.php',
                'compare' => '!='
                )
            ),
        ));

        query_posts( $args );

    }

}
add_filter('pre_get_posts','exclude_page_templates_from_search');



