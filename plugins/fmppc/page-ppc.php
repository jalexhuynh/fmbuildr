<?php
/* Template Name: PPC */
include_once('header-ppc.php'); ?>
 	<div class="row">
		<div class="col-md-12"><h1><?php the_title(); ?></h1></div>
	</div><hr>	 	
	<div class="row"> 
				<div class="col-md-8 mainpostcont">					
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
			  	<?php the_content(); ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>		
		</div>
		<div class="col-md-4">	 
			<?php include_once('sidebar-ppc.php');  ?>	
		</div>
	</div><!-- end of posts row -->
	<?php 		
		if(have_rows('layouts')): //CHECK LAYOUTS 	
			while(have_rows('layouts')): the_row(); //GET ALL LAYOUTS FOR MAIN FLEXIBLE CONTENT
				  if(get_row_layout() == 'two_column_gallery'): //GET TWO COLUMN GALLERY ELEMENTS ?>
				  	<div class="row">
				  	<?php if(get_sub_field('two_column_heading')): ?>
				  		<div class="col-md-12"><h2><?php the_sub_field('two_column_heading'); ?></h2></div>
				  	<?php endif; ?>
				  	</div>	
				  	<div class="row ppcGalleryWrap">
				  	<?php 
						$ppc_images = get_sub_field('two_column_gallery');
						if($ppc_images):
								$itmctr = 1;
								$itemcol = "";
							foreach( $ppc_images as $ppc_image):  
								  if (($itmctr % 2) == 1)
									  { $itemcol = "before";}
								  else
									  { $itemcol = "after";}							
							?>							
								<div class="col-md-3 col-xs-6 ppc_col_<?=$itemcol?>" id="ppc_col_item<?=$itmctr?>">
										<!-- ENABLE WHEN IT NEEDS LINK <a href="<?=$ppc_image['url']?>"> -->
										<img src="<?=$ppc_image['sizes']['ppc_medium']?>" class="ppc_gal_img" alt="<?=$ppc_image['alt']?>">
										<!-- ENABLE WHEN IT NEEDS LINK </a> -->
								</div>	
						<?php	$itmctr++; ?>
						<?php endforeach;
						endif;	
				  	?>					  		
				  	</div>
				  	
					  		<?php if(have_rows('call_to_action')): 
									while(have_rows('call_to_action')): the_row() ?>
							<div class="row">		
								<div class="col-md-12"><p align="<?php the_sub_field('alignment'); ?>"><a href="<?php the_sub_field('cta_link');?>" class="btn btn-default ppc-btn" target="_blank" ><?php the_sub_field('cta_text'); ?></a></p></div>
									</div>	
							<?php	endwhile; // END OF GET ALL CTA ELEMENT
							 	endif; //END OF CHECK CTA ELEMENT ?>
				  
			<?php elseif(get_row_layout() == 'add_horizontal_line'): ?>
					<hr class="ppc_hr" style="border-color:<?php the_sub_field('color') ?>!important;">	 	
			<?php elseif(get_row_layout() == 'two_column_with_sidebar')://GET TWO COLUMN WITH SIDEBAR ?>
					<div class="row">
				  	<?php if(get_sub_field('two_column_with_sidebar_heading')): ?>
						<div class="col-md-12"><h2><?php the_sub_field('two_column_with_sidebar_heading'); ?></h2></div>
				  	<?php endif; ?>						
						<div class="col-md-8"><?php
							//START FLEXIBLE CONTENT FOR COLUMNS
							if(have_rows('content_area_container')):
								while(have_rows('content_area_container')): the_row();
									if(get_row_layout() == 'content_field'):
									 ?> <div class="twocol_txt_content"><?php the_sub_field('content_txt_field'); ?></div> <?
									elseif(get_row_layout() == 'accordion'):
									 	if(get_sub_field('accordion_title')):	?>											
											<h3 align="left"><?php the_sub_field('accordion_title'); ?></h3>
										<?php endif;	
										 if(have_rows('accordion_content')): ?>
										 	<?php $count = rand(); ?>
										 	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">													  
											<? while(have_rows('accordion_content')): the_row(); ?>
											     <div class="panel panel-default">
												    <div class="panel-heading" role="tab" id="heading<?=$count; ?>">
												      <h4 class="panel-title">
												        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$count; ?>" aria-expanded="true" aria-controls="collapse<?=$count;?>">
												          <?php the_sub_field('accordion_list_title'); ?>
												        </a>
												      </h4>
												    </div>
												    <div id="collapse<?=$count;?>" class="panel-collapse collapse<?php if($count == 1): echo ' in'; endif;?>" role="tabpanel" aria-labelledby="heading<?=$count; ?>">
												      <div class="panel-body">
														<?php the_sub_field('accordion_list_content'); ?>
												      </div>
												    </div>
												  </div>  
											<?php $count++; ?>	    													  												
											<?php endwhile;//end of accordion content ?>													
											</div>	
										<? endif;//end of accordion content
										elseif(get_row_layout() == 'gallery'): ?>								
											<div class="row ppcGalleryWrap">
												<?php 
												$ppc_images = get_sub_field('gallery');
													if($ppc_images):
														$itmctr = 1;
														$itemcol = "";
														foreach( $ppc_images as $ppc_image):  
														if (($itmctr % 2) == 1)
														{ $itemcol = "before";}
														else
														{ $itemcol = "after";}							
														?>							
														<div class="col-md-6 col-xs-6 ppc_col_<?=$itemcol?>" id="ppc_col_item<?=$itmctr?>">
															<!-- ENABLE WHEN IT NEEDS LINK <a href="<?=$ppc_image['url']?>"> -->
															<img src="<?=$ppc_image['sizes']['ppc_medium']?>" class="ppc_gal_img" alt="<?=$ppc_image['alt']?>">
															<!-- ENABLE WHEN IT NEEDS LINK </a> -->
														</div>	
														<?php	$itmctr++; ?>
													<?php endforeach;
													endif;	
												?>					  		
											</div>								
										<?php elseif(get_row_layout() == 'cta_link_button'): ?>
												<?php if(have_rows('call_to_action')): 
									while(have_rows('call_to_action')): the_row() ?>
								<p align="<?php the_sub_field('alignment'); ?>"><a href="<?php the_sub_field('cta_link');?>" class="btn btn-default ppc-btn" target="_blank" ><?php the_sub_field('cta_text'); ?></a></p>
							<?php	endwhile; // END OF GET ALL CTA ELEMENT
							 	endif; //END OF CHECK CTA ELEMENT ?>
									<? endif; //end of content area container										
								endwhile; //end of content area container
							endif;
							//END OF FLEXIBLE CONTENT FOR COLUMNS	
						?></div>
						<div class="col-md-4">
							<?php $ctr = 1; ?>
							<?php if(have_rows('sidebar_content')): 
									while(have_rows('sidebar_content')):the_row(); ?>
										<div class="sidebarWidget<?=$ctr;?>"><?php the_sub_field('sidebar_widget'); ?></div>
							 <?php	   		$ctr++;
								 	endwhile;
								  endif;   										
							 ?>
						</div>
					</div>			
			<? elseif(get_row_layout() == 'narrow_single_column'): //GET NARROW COLUMN ELEMENTS ?>
			<?php 
				if(get_sub_field('layout_width') == 'center' ):
					$layoutClass = "col-md-8 col-md-offset-2";
			   elseif(get_sub_field('layout_width') == 'full'):
			   		$layoutClass = "col-md-12";
			   endif;	
			 ?>	
				 	<div class="row">					 	
						<div class="<?=$layoutClass ?>">
							<!-- STYLE OF Single COL -->
							<?php if(get_sub_field('single_col_sub_heading')): ?>
							<style> #singleColH2 { margin-bottom:0 !important; } #singlecolSub {margin-bottom: 25px;}</style>
							<?php endif; ?>
							<h2 id="singleColH2" align="<?php the_sub_field('heading_alignment'); ?>"><?php the_sub_field('narrow_single_column_heading') ?></h2>
							<?php if(get_sub_field('single_col_sub_heading')): ?>
								<div id="singlecolSub"><?php the_sub_field('single_col_sub_heading'); ?></div>
							<? endif; ?>
						</div>						
					</div>
					<div class="row">					
						<div class="<?=$layoutClass ?>">
							<?php 
								//START FLEXIBLE CONTENT FOR COLUMNS
								if(have_rows('content_area_container')):
									while(have_rows('content_area_container')): the_row();
										if(get_row_layout() == 'content_field'):
											 ?> <div class="sngle_txt_content"><?php the_sub_field('content_txt_field'); ?></div> <?
										elseif(get_row_layout() == 'accordion'):
										 	if(get_sub_field('accordion_title')):	?>											
												<h3 align="left"><?php the_sub_field('accordion_title'); ?></h3>
											<?php endif;	
											 if(have_rows('accordion_content')): ?>
											 	<?php $count = rand(); ?>
											 	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">													  
												<? while(have_rows('accordion_content')): the_row(); ?>
												     <div class="panel panel-default">
													    <div class="panel-heading" role="tab" id="heading<?=$count; ?>">
													      <h4 class="panel-title">
													        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$count; ?>" aria-expanded="true" aria-controls="collapse">
													          <?php the_sub_field('accordion_list_title'); ?>
													        </a>
													      </h4>
													    </div>
													    <div id="collapse<?=$count;?>" class="panel-collapse collapse<?php if($count == 1): echo ' in'; endif;?>" role="tabpanel" aria-labelledby="heading<?=$count; ?>">
													      <div class="panel-body">
															<?php the_sub_field('accordion_list_content'); ?>
													      </div>
													    </div>
													  </div>  
												<?php $count++; ?>	    													  												
												<?php endwhile;//end of accordion content ?>													
												</div>	
											<? endif;//end of accordion content
											elseif(get_row_layout() == 'gallery'): ?>								
												<div class="row ppcGalleryWrap">
													<?php 
													$ppc_images = get_sub_field('gallery');
														if($ppc_images):
															$itmctr = 1;
															$itemcol = "";
															foreach( $ppc_images as $ppc_image):  
															if (($itmctr % 2) == 1)
															{ $itemcol = "before";}
															else
															{ $itemcol = "after";}							
															?>							
															<div class="col-md-6 col-xs-3 ppc_col_<?=$itemcol?>" id="ppc_col_item<?=$itmctr?>">
																<!-- ENABLE WHEN IT NEEDS LINK <a href="<?=$ppc_image['url']?>"> -->
																<img src="<?=$ppc_image['sizes']['ppc_medium']?>" class="ppc_gal_img" alt="<?=$ppc_image['alt']?>">
																<!-- ENABLE WHEN IT NEEDS LINK </a> -->
															</div>	
															<?php	$itmctr++; ?>
														<?php endforeach;
														endif;	
													?>					  		
												</div>									
									<?	endif; //end of content area container										
									endwhile; //end of content area container
								endif;
								//END OF FLEXIBLE CONTENT FOR COLUMNS
							?>
							<?php if(have_rows('call_to_action')): 
									while(have_rows('call_to_action')): the_row() ?>
								<p align="<?php the_sub_field('alignment'); ?>"><a href="<?php the_sub_field('cta_link');?>" class="btn btn-default ppc-btn" target="_blank" ><?php the_sub_field('cta_text'); ?></a></p>
							<?php	endwhile; // END OF GET ALL CTA ELEMENT
							 	endif; //END OF CHECK CTA ELEMENT ?>
							 	
						</div>								
					</div>
	<?php		endif;			
			endwhile; //END OF GET ALL LAYOUTS
		endif;	//END OF CHECK LAYOUTS
	?>	
<?php include_once('footer-ppc.php'); ?>