<?php
/**
 * Plugin Name: FM Reviews Template
 * Plugin URI: https://firm-media.com
 * Description: This plugin adds a reviews template with acf fields with sorting capability. Needs FM isotope and ACF pro fields plugin to fully function
 * Version: 1.0.0
 * Author: Firm Media - Dev Team
 * Author URI: https://firm-media.com
 * License: GPL2
 */
 
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5c06c914748c0',
	'title' => 'Reviews Plugin',
	'fields' => array(
		array(
			'key' => 'field_5c06c924a7241',
			'label' => 'Reviews',
			'name' => 'reviews',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5c06ca76a7242',
			'min' => 0,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Add A Review',
			'sub_fields' => array(
				array(
					'key' => 'field_5c06cb24a7248',
					'label' => 'Status',
					'name' => 'status',
					'type' => 'radio',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'publish' => 'Publish',
						'draft' => 'Draft',
					),
					'allow_null' => 0,
					'other_choice' => 0,
					'default_value' => '',
					'layout' => 'horizontal',
					'return_format' => 'value',
					'save_other_choice' => 0,
				),
				array(
					'key' => 'field_5c06ca76a7242',
					'label' => 'Reviewer Name',
					'name' => 'reviewer_name',
					'type' => 'text',
					'instructions' => 'You can minimize this review entry by hovering & pressing the arrow on the top left corner.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5c06ca9aa7244',
					'label' => 'Source URL',
					'name' => 'source_url',
					'type' => 'text',
					'instructions' => 'Attach a URL link to the original Review (if located in a different site)',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5c06ca8ba7243',
					'label' => 'Reviews',
					'name' => 'reviews',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'field_5c06caf7a7246',
					'label' => 'Source / Category',
					'name' => 'source_category',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'return_format' => 'value',
					'ajax' => 0,
					'placeholder' => '',
				),
				array(
					'key' => 'field_5c06cb13a7247',
					'label' => 'Procedure Name',
					'name' => 'procedure_name',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'return_format' => 'value',
					'ajax' => 0,
					'placeholder' => '',
				),
			),
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_template',
				'operator' => '==',
				'value' => 'page-review.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'discussion',
		3 => 'comments',
		4 => 'featured_image',
		5 => 'categories',
		6 => 'tags',
	),
	'active' => 1,
	'description' => '',
));



acf_add_local_field_group(array (
	'key' => 'group_586d437af2021',
	'title' => 'Reviews Plugin Options',
	'fields' => array (
		array (
			'key' => 'field_586d5312687f6',
			'label' => 'Settings',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d53cc687f8',
			'label' => 'Font Color',
			'name' => 'font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d533a687f7',
			'label' => 'Top Margin',
			'name' => 'top_margin',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_586d53f1687f9',
			'label' => 'Font Size',
			'name' => 'font_size',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_586d88ba041a8',
			'label' => 'Default Site Logo',
			'name' => 'rev_site_logo',
			'type' => 'image',
			'value' => NULL,
			'instructions' => 'Default Fallback for site\'s square logo. ( uncategorized source )',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_586fdaaffbba8',
			'label' => 'Sources and Procedures',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586fd56ed44a0',
			'label' => 'Sources',
			'name' => 'sources',
			'type' => 'repeater',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => '',
			'collapsed' => '',
			'sub_fields' => array (
				array (
					'key' => 'field_586fd6b04a571',
					'label' => 'Value',
					'name' => 'value',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
				array (
					'key' => 'field_586fd6b64a572',
					'label' => 'Label',
					'name' => 'label',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
			),
		),
		array (
			'key' => 'field_586fd576d44a1',
			'label' => 'Procedures',
			'name' => 'procedures',
			'type' => 'repeater',
			'value' => NULL,
			'instructions' => 'Each entries should be one line each.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => '',
			'collapsed' => '',
			'sub_fields' => array (
				array (
					'key' => 'field_586fd6cc4a573',
					'label' => 'Value',
					'name' => 'value',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
				array (
					'key' => 'field_586fd6d14a574',
					'label' => 'Label',
					'name' => 'label',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
			),
		),
		array (
			'key' => 'field_586d47c3e8e35',
			'label' => 'Button',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d438949b1e',
			'label' => 'Button Font Color',
			'name' => 'button_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d43ac49b1f',
			'label' => 'Button Background Color',
			'name' => 'button_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#eeeeee',
		),
		array (
			'key' => 'field_586d43e349b20',
			'label' => 'Button Hover Font Color',
			'name' => 'button_hover_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d444e49b21',
			'label' => 'Button Hover Background Color',
			'name' => 'button_hover_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#99b8e1',
		),
		array (
			'key' => 'field_586d44ab49b22',
			'label' => 'Button Active Font Color',
			'name' => 'button_active_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d44df49b23',
			'label' => 'Button Active Background Color',
			'name' => 'button_active_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#43556c',
		),
		array (
			'key' => 'field_586d4821855e6',
			'label' => 'Button Mobile',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d464549b24',
			'label' => 'Button Mobile Font Color',
			'name' => 'button_mobile_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d474349b25',
			'label' => 'Button Mobile Background Color',
			'name' => 'button_mobile_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#8da8af',
		),
		array (
			'key' => 'field_586d475b49b26',
			'label' => 'Button Mobile Hover Font Color',
			'name' => 'button_mobile_hover_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d477849b27',
			'label' => 'Button Mobile Hover Background Color',
			'name' => 'button_mobile_hover_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#a7c7cf',
		),
		array (
			'key' => 'field_586fdaf83e9c6',
			'label' => 'Custom CSS rewrites',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586fdac5fbba9',
			'label' => 'Custom CSS rewrites',
			'name' => 'custom_css_rewrites',
			'type' => 'textarea',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'new_lines' => '',
			'maxlength' => '',
			'placeholder' => '',
			'rows' => 10,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'theme-reviews',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

//END OF REVIEW PAGE TEMPLATE 
function reviews_scripts() {
	wp_enqueue_style( 'reviews-style', plugin_dir_url( __FILE__ ) . 'css/compiled/style.css', array() );
	wp_enqueue_style( 'custom-style', plugin_dir_url( __FILE__ ) . 'css/custom.css', array() );
// 	wp_enqueue_style( 'custom-foundation-style', plugin_dir_url( __FILE__ ) . 'css/compiled/foundation.min.css', array() );
	wp_enqueue_script( 'fm-isotope-js', plugins_url( 'js/fm-isotope-all.min.js', __FILE__ ) , array( 'jquery' ), 'null', true );
	//wp_enqueue_script( 'fm-isotope', plugins_url( 'js/isotope.pkgd.min.js', __FILE__ ), array('jquery'), '2.2.0', true );	
	//wp_enqueue_script( 'fm-isotope', plugins_url( 'js/isotope.pkgd.js', __FILE__ ), array( 'jquery' ), '2.2.0', true );
	//wp_enqueue_script( 'fm-isotope-js', plugins_url( 'js/fm-isotope.js', __FILE__ ) , array( 'jquery' ), 'null', true );	
	//wp_enqueue_script('custom-js', plugin_dir_url( __FILE__ ) . 'js/script.js', array( 'jquery' ),'', true );

}



add_action( 'wp_enqueue_scripts', 'reviews_scripts' );


add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'review' ) {
        if ( is_archive() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'archive-review.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/archive-review.php';
            }
        }
    }
    return $template_path;
}

function limit_words($string, $word_limit) {
	$words = explode(" ", $string);
	return implode(" ",array_splice($words, 0 , $word_limit));
}

add_action( 'pre_get_posts', 'my_post_queries' );
function my_post_queries( $query ) {

    if( is_post_type_archive( 'review' ) && !is_admin() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 1000 );        
    }

}

if( function_exists('acf_add_options_page') ) {	
	
	 acf_add_options_page(array(
		'page_title' 	=> 'Reviews Settings',
		'menu_title'	=> 'Reviews Settings',
		'menu_slug' 	=> 'theme-reviews',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' => 'dashicons-images-alt2'
	));	
}


function acf_sources_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // if has rows
    if( have_rows('sources', 'options') ) {
        
        // while has rows
        while( have_rows('sources', 'options') ) {            
            // instantiate row
            the_row();            
            // vars
            $value = get_sub_field('value');
            $label = get_sub_field('label');            
            // append to choices
            $field['choices'][ $value ] = $label;            
        }        
    }
    // return the field
    return $field;    
}

add_filter('acf/load_field/key=field_5c06caf7a7246', 'acf_sources_field_choices');


function acf_procedures_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // if has rows
    if( have_rows('procedures', 'options') ) {
        
        // while has rows
        while( have_rows('procedures', 'options') ) {            
            // instantiate row
            the_row();            
            // vars
            $value = get_sub_field('value');
            $label = get_sub_field('label');            
            // append to choices
            $field['choices'][ $value ] = $label;            
        }        
    }
    // return the field
    return $field;    
}

add_filter('acf/load_field/key=field_5c06cb13a7247', 'acf_procedures_field_choices');

add_theme_support( 'post-thumbnails', array( 'post','review' ) ); // reviews 


class ReviewsPageTemplater {

	/**
	 * A reference to an instance of this class.
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Returns an instance of this class. 
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new ReviewsPageTemplater();
		} 

		return self::$instance;

	} 

	/**
	 * Initializes the plugin by setting filters and administration functions.
	 */
	private function __construct() {

		$this->templates = array();


		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}

		// Add a filter to the save post to inject out template into the page cache
		add_filter(
			'wp_insert_post_data', 
			array( $this, 'register_project_templates' ) 
		);


		// Add a filter to the template include to determine if the page has our 
		// template assigned and return it's path
		add_filter(
			'template_include', 
			array( $this, 'view_project_template') 
		);


		// Add your templates to this array.
		$this->templates = array(
			'page-review.php' => 'Reviews',
		);
			
	} 

	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
	 */
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. 
		// If it doesn't exist, or it's empty prepare an array
		$templates = wp_get_theme()->get_page_templates();
		if ( empty( $templates ) ) {
			$templates = array();
		} 

		// New cache, therefore remove the old one
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	} 

	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		
		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta( 
			$post->ID, '_wp_page_template', true 
		)] ) ) {
			return $template;
		} 

		$file = plugin_dir_path( __FILE__ ). get_post_meta( 
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

} 
add_action( 'plugins_loaded', array( 'ReviewsPageTemplater', 'get_instance' ) );



 ?>