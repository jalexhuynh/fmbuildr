<?php get_header(); 
/* 
	Template Name: Reviews 
*/
?>
<style>
	<?php the_field( 'custom_css_rewrites','options' ); ?>
	
	.revContent {
		color:<?php echo get_field('font_color','options') ?> !important;
		font-size:<?php echo get_field('font_size','options') ?> !important;
		margin-top: <?php echo get_field('top_margin','options') ?> !important; 
	}
	.revContent .reviews-content .button {
		background-color:<?php echo get_field('button_background_color','options'); ?> !important;
		color:<?php echo get_field('button_font_color','options'); ?> !important;
	}
	.revContent .reviews-content .button:hover {
		background-color:<?php echo get_field('button_hover_background_color','options'); ?> !important;
		color:<?php echo get_field('button_hover_font_color','options'); ?> !important;		
	}
	.revContent .reviews-content .button.is-checked {
		background-color:<?php echo get_field('button_active_background_color','options'); ?> !important;
		color:<?php echo get_field('button_active_font_color','options'); ?> !important;		
	}
	.revContent .reviews-content .button.is-main {
		background-color:<?php echo get_field('button_mobile_background_color','options'); ?> !important;
		color:<?php echo get_field('button_mobile_font_color','options'); ?> !important;	
		border: 1px #eee solid !important;	
	}
	.revContent .reviews-content .button.is-main:hover {
		background-color:<?php echo get_field('button_mobile_hover_background_color','options'); ?> !important;
		color:<?php echo get_field('button_mobile_hover_font_color','options'); ?> !important;	
	}
	.nopad, .noPad {
		padding:0 !important;
	}
	@media (max-width:1025px) {
		.banner-bg {
			margin-top:0 !important;
		}
	}

	
</style>
<?php
	$rSource = array();
	$rProcedures = array();					
	$total_posts = 0;
									 							  
	if (have_rows('reviews')) : while (have_rows('reviews')) : the_row();
		$status = get_sub_field('status');		
			if($status == 'publish'):
				$source = get_sub_field('source_category');
				$sfield = get_sub_field_object('source_category');								
				$slabel = $sfield['choices'][ $source ];
				
				$procedure = get_sub_field('procedure_name');
				$pfield = get_sub_field_object('procedure_name');								
				$plabel = $pfield['choices'][ $procedure ];
					
				$rProcedures[$procedure] = $plabel;
				$rSource[$source] = $slabel;
				
				$total_posts++;	
			endif;		
		endwhile;
	endif;
	//wp_reset_postdata();							  	
?>

<?php // set up page banner acf fields 
$page_for_posts = get_option( 'page_for_posts', true );
if (is_home()){
	$post_id = 	$page_for_posts;
}  else {
	$post_id = $post->ID;
}
	
if(is_home() || is_archive() || is_404()):	
	$bannerBgImgs = get_field('banner_background_imgs','options');
	$bgColor = get_field('background_color','options');
	$minHeight = get_field('banner_min_height','options');
else :
	$bannerBgImgs = get_field('banner_background_imgs');
	$bgColor = get_field('background_color');
	$minHeight = get_field('banner_min_height');
endif;


$altTitle = get_field('alt_title', $post_id);
$geoTag = get_field('geo_tag' , $post_id);
$removeme =  array('Category:','Archives:');
 
if ($bannerBgImgs):
/* loop through the images in the banner background images gallery field and save them as an array 
** for use in our data-interchange background src.  
*/?>
	<?php $ctr = 0; foreach($bannerBgImgs  as $image ): $ctr++; ?>							 
		<?php $bannerimgs[$ctr] = $image['url']; ?>					
	<?php endforeach; ?>
<?php endif;?>

<section id="page-banner" style="min-height: <?=$minHeight;?>">
	<div class="banner-bg banner-parallax animate fadeIn" data-speed="0.3" <?php if ($bannerBgImgs):?>  style="background-size: cover; background-repeat: no-repeat;" data-interchange="[<?=$bannerimgs[3];?>, small], [<?=$bannerimgs[1];?>, xmedium], [<?=$bannerimgs[1];?>, large]" <?php else:?> style="background-color: <?=$bgColor;?>;" <?php endif;?>>
		<div class="grid-container">
			<div class="grid-x grid-padding-x align-center-middle align-center align-middle banner-content" style="min-height:<?=$minHeight;?>">
				<div class="cell small-12 medium-10 large-10 text-center">
					<?php if(is_home()): ?>
						<h1 class="page-title tagline"><?php if($altTitle) { echo $altTitle ;} else { echo get_the_title($post_id); }?> <?php if($geoTag) { echo'<small class="sub-header">' . $geoTag . '</small>';}?></h1>
					<?php elseif(is_archive()): ?>
						<h1 class="page-title tagline"><?php echo str_replace($removeme, "", get_the_archive_title()); ?></h1>
					<?php elseif(is_404()): ?>
						<h1 class="page-title tagline">Page not found</h1>
					<?php  else:?>
						<h1 class="page-title tagline"><?php if($altTitle) { echo $altTitle ;} else { the_title();}?> <?php if($geoTag) { echo'<small class="sub-header">' . $geoTag . '</small>';}?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>	
	</div>		
</section>
<?php 	// This can be found @ Admin > Appearance > Customize > Sub Pages > Choose Sub Page Template
	$template_type = get_theme_mod("fmbuildr_subpage_type"); 
	switch ($template_type) {
    case "two_col_grid":
      	$grid_attr ="";
        $cell_attr = "small-12 medium-8 large-8";      
        break;
    case "full_width_grid_10":
    	$grid_attr ="align-center"; 
        $cell_attr = "small-12 medium-10 large-10";               
        break; 
    case "full_width_grid":
        $cell_attr = "small-12 medium-12 large-12";
        $grid_attr =  "";       
        break;}		
?>
<div id="content" class="revContent ">
	<div class="grid-x grid-container grid-padding-x <?php echo $grid_attr; ?>" style="padding:3em 0 4em">
		<div class="<?php echo $cell_attr; ?> cell reviews-content" role="main" >	
		    <div class="grid-x">
			    <div class="cell small-12 text-right"><p><em>*Individual results may vary</em></p></div>
		          <div class="large-3 medium-4 small-12 cell source_cont">				        	
					<div class="button-group filter-button-group">								  
						  <!-- FILTER FOR LARGE , MEDIUM AND SMALL -->
						  <div class="filters " style="width:100%">
							  	<input type="button" class="button is-main source-btn hide-for-medium" value="Filter by Source »" data-filter="" >
							  <div class="ui-group" style="margin-bottom:5px;" >
								  <h3 class="show-for-medium">Source</h3>											  
								  <div  id="sourcewrap"  class="button-groups hideShow js-radio-button-group2" data-filter-group="source">
									   <button class="button is-checked"  data-filter="">Show All (<?php echo $total_posts?>)</button><?php
								  	foreach ($rSource as $key => $value){ ?>
									  	<button class="button inner " data-filter=".<?=$key?>"><?=$value?></button>
								  <?php	} ?>
								  </div>
						      </div>
						      <input type="button" class="button is-main secondary procedure-btn hide-for-medium" value="Filter by Procedure »" data-filter="" >
						      <div class="ui-group">		   
								 <h3 class="show-for-medium" style="margin-top:20px;">Procedure</h3> 
								 <div  id="procedurewrap" class="button-groups js-radio-button-group1" data-filter-group="procedure">
									 	<button class="button is-checked" data-filter="">Show All (<?php echo $total_posts?>)</button> <?php 
									  foreach ($rProcedures as $key => $value){ ?>
									  	<button class="button  inner " data-filter=".<?=$key?>"><?=$value?></button>
								  <?php	} ?>
								 </div>
						      </div>
						  </div>								    				 
					</div>
				</div>
		          <div class="large-9 medium-8 small-12 cell grid nopad">
			          
			          
					<?php if (have_rows('reviews')) : while (have_rows('reviews')) : the_row();  
						$source = get_sub_field('source_category');
						$procedure = get_sub_field('procedure_name');
						$source_url = get_sub_field('source_url');
						$status = get_sub_field('status');		
							if($status == 'publish'):	
					?>						
						<div class="element-item fade-in <?=$source?> <?=$procedure?>" data-category="<?=$source?>" id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>		
							<div class="grid-x">
								<div class="small-4 medium-5 large-3 cell rating-stars-wrapper">
								<div class="rating-stars">
									<div>
										<i class="fa fa-star gold"></i>
										<i class="fa fa-star gold"></i>
										<i class="fa fa-star gold"></i>
										<i class="fa fa-star gold"></i>
										<i class="fa fa-star gold"></i>
									</div>
									<div class="rating-text">
										<strong>(5 out of 5 stars)</strong>
									</div>												
								</div>

								<?php
									if($source != 'uncategorized'):											
									echo '<a href="'.$source_url.'" rel="nofollow" target="_blank"><img alt="review source" class="rev_img" src="' . plugins_url( 'images/'.$source.'.png', __FILE__ ) . '" ></a>';
									else:
									$rev_site_logo = get_field('rev_site_logo','options');
									if($rev_site_logo) {
										echo '<img class="rev_img" src="'. $rev_site_logo['url'] .'" alt="uncategorized">';

									} else {
										echo '<img class="rev_img" src="'. plugins_url( 'images/uncategorized.png', __FILE__ ) .'" alt="uncategorized">';
									}
								
									endif;	
									?>								
							</div>
								<div class="small-8 medium-7 large-9 cell excerpts_cont">
							
								<div class="source-date-review">
									
									<?php 
										$source = get_sub_field('source_category');
								  		$field = get_sub_field_object('source_category');								
										$label = $field['choices'][ $source ];
										$reviews = get_sub_field('reviews');
										$review_from = get_sub_field('reviewer_name');											
// 												$date_posted = get_field('date_posted');
										$classlbl = rand();
										if($review_from) : 
											_e('Review from '); echo "<span class='review-from'>".$review_from."</span>  ";
										endif;
										if($source_url):
											_e('| Source: '); echo "<span rel='nofollow'>".$label ."</span> | ";
										endif;
/*
										if($date_posted):
											echo $date_posted;
										endif;
*/
									the_date('M d, Y');
									?>	
								</div>
								
								<?php 
									$strlen = $reviews;
									$fader = ""; 
									if(strlen($strlen) > 250){ 
										$fader = "fader";
									}
									
								?>							
								
								<div class="revtext short <?=$classlbl?> <?=$fader?>">	
								<?php //echo limit_words($strlen, 30)." ....";
									echo $strlen;							
								$single_source_url = get_field('single_source_url'); ?>
								
								</div>									
								<?php if(strlen($strlen) > 250){ ?>	
																																																	<a id="more" class="fm-read-more secondary link more-<?=$classlbl?> togg-<?=$classlbl?>" rel="<?=$classlbl?>" data-toggles="true" > Read more »</a>
								<a id="less" class="fm-read-more  secondary link- less-<?=$classlbl?> togg-<?=$classlbl?>" style="display:none;" rel="<?=$classlbl?>" data-toggles="true"> Read less »</a>
								<?php } ?>
							</div>												
							</div>													
						</div>						<?php	
							endif;
								endwhile; ?>
					<?php endif; ?>					    	
		    	</div>  
	          </div>      
		</div> 					
		<?php if($template_type == "two_col_grid"): ?>
			<div id="reviews-sidebar" class="reviews-sidebar large-4 medium-4 small-12 cell" role="complementary">
			

			<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>
		
				<?php dynamic_sidebar( 'sidebar1' ); ?>
		
			<?php else : ?>
		
			
								
			<div class="alert help">
				<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
			</div>
		
			<?php endif; ?>
		
		
		</div> 
		<?php endif; ?>
	</div>           
</div>
<?php get_footer(); ?>