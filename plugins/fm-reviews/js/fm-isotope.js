jQuery(window).on('load', function(){
	setTimeout(function(){
		initMasonry();	
	}, 10);
});

// init masonry
function initMasonry(){
	jQuery('.iWrap').css({"margin": "15px 0"});
		jQuery('#fm-isotope').isotope({
			itemSelector: '.iWrap',
			masonry: {
			columnWidth: '.iWrap',
			layoutMode: 'fitColumns'
		}
	});
}