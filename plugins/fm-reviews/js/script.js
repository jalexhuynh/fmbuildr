// external js: isotope.pkgd.js

jQuery(document).ready( function() {
	
  // init Isotope
  var $grid = jQuery('.grid').isotope({
    itemSelector: '.element-item'
  });

  // store filter for each group
  var filters = {};

  jQuery('.filters').on( 'click', '.button', function() {
    var $this = jQuery(this);
    // get group key
    var $buttonGroup = $this.parents('.button-group');
    var filterGroup = $buttonGroup.attr('data-filter-group');
    // set filter for group
    filters[ filterGroup ] = $this.attr('data-filter');
    // combine filters
    var filterValue = concatValues( filters );
    // set filter for Isotope
    $grid.isotope({ filter: filterValue });
  });

  // change is-checked class on buttons
  jQuery('.js-radio-button-group2').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });
  
    // change is-checked class on buttons
  jQuery('.js-radio-button-group1').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });
  
});

// flatten object by concatting values
function concatValues( obj ) {
  var value = '';
  for ( var prop in obj ) {
    value += obj[ prop ];
  }
  return value;
}

// target grid name.
 var $grid = jQuery('.grid').isotope({
    itemSelector: '.element-item'
  });

triggerClick = true;
jQuery(".fm-read-more").click(function() {
	var toggie = jQuery(this).attr("data-toggles");
	
	if (toggie === "true") {
		var e = jQuery(this).attr("rel"),
			t = jQuery("." + e).css("height", "auto").height();
		jQuery("." + e).animate({
			height: t
		}, 500), $grid.isotope("layout"), 
		jQuery(".togg-" + e).attr("data-toggles", "false");
		jQuery(".more-" + e).hide();
		jQuery(".less-" + e).show();
		jQuery("." + e).removeClass('short');
	} else {
		var e = jQuery(this).attr("rel");
		jQuery(".less-" + e).hide();
		jQuery(".more-" + e).show();
		jQuery("." + e).animate({
			height: 100
		}, 200), setTimeout(function() {
			$grid.isotope("layout")
		}, 300), jQuery(".togg-" + e).attr("data-toggles", "true");
		jQuery("." + e).addClass('short');
	}
}), jQuery(window).resize(function() {
	jQuery(".revtext").animate({
		height: 100
	}, 200), setTimeout(function() {
		$grid.isotope("layout"), triggerClick = true;
	}, 300),
	jQuery(".fm-read-more").attr("data-toggles", "true");
	jQuery("#less").hide();
	jQuery("#more").show();
	jQuery(".revtext").addClass('short');
});

