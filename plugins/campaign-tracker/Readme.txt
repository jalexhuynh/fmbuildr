=-=-=-=-= =-=-=-=-= Campaign Tracker =-=-=-=-= =-=-=-=-= =-=-=-=-= 

Change  Log

Version 2.0

- Support for Ninja Forms 3.x

Version 1.9

+ Improvements in licence management

Version 1.8

+ Improved licence management 

Version 1.6.5

- Updated method used to collect website referrer

Version 1.6

+ New support for contact form 7

Version 1.5

+ URL builder added to the WordPress editing interface
+ New settings for storing default for campaign variables

Version 1.0

+ initial version.

=-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-= =-=-=-=-=


