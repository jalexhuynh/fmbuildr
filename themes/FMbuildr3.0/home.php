<?php 
/* 
	default template for main posts page or blog
*/ 
get_header(); 
get_template_part('parts/components/component', 'banner');

	// This can be found @ Admin > Appearance > Customize > Sub Pages > Choose Sub Page Template
	$template_type = get_theme_mod("fmbuildr_archive_type"); 
	$container_attr = "grid-container";
	switch ($template_type) {
    case "two_col_grid":
      	$grid_attr ="grid-padding-x";
        $cell_attr = "small-12 medium-8 large-8";      
        break;
    case "full_width_grid_10":
    	$grid_attr ="align-center grid-padding-x"; 
        $cell_attr = "small-12 medium-12 large-10";               
        break; 
    case "full_width_grid":
        $cell_attr = "small-12 medium-12 large-12";
        $grid_attr =  "grid-padding-x";       
        break;
    case "full_width_grid_nc":
        $cell_attr = "small-12 medium-12 large-12";
        $grid_attr =  "";
        $container_attr = "";       
		break;}	    	
?>

<div class="content" id="content">

	<div class="<?=$container_attr?>">		

		<div class="inner-content grid-x <?php echo $grid_attr; ?>">		

		    <main class="main cell endless-scroll <?php echo $cell_attr; ?>" role="main">			  
				
				<!--- // Disabled for endless scrolling. left commented if it needs to be reverted to classic pagination
					Posts endless scrolling is handled by:
					1. JS: /assets/scripts/additional-scripts/load-more.js that targets the class name "endless-scroll"
					2. STYLE: /styles/scss/global.scss ---- line 925
					3. PHP : /functions/theme-support.php -- line 459 be_ajax_load_more()
					4. PHP : /functions/enqueue-scripts.php --- line 49 be_load_more_js()					 
					
		    	<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					
					<?php //get_template_part( 'parts/loops/loop', 'archive-excerpt' ); ?>
				    
				<?php //endwhile; ?>	

					<?php //joints_page_navi(); ?>
					
				<?php //else : ?>
											
					<?php //get_template_part( 'parts/contents/content', 'missing' ); ?>
						
				<?php //endif; ?>	
				--->	

			
			</main> <!-- end #main -->
	
			<?php if($template_type == "two_col_grid"): get_sidebar(); endif; ?>
	    
	    </div> <!-- end #inner-content -->
	    
	</div> <!-- end #content -->

</div> <!-- end .grid-container -->

<?php get_footer(); ?>