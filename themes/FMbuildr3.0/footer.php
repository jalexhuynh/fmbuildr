<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
// set up addtional footer acf fields 
$additionalFooter = get_field('footer_toggler','options');
$addtionalFooterContent = get_field('footer_content','options');
$footerbg = get_field('footer_background_images','options');
$contentLeft  = get_field('content_left','options');
$contentRight = get_field('content_right','options');

	if ($footerbg):
	/* loop through the images in the banner background images gallery field and save them as an array 
	** for use in our data-interchange background src.  
	** dataInterchangeImgs() can be found on assets/functions/theme-support.php --- line 75
	*/
		$dataInterchange = dataInterchangeImgs($footerbg);
	endif;

?>				<footer class="footer" role="contentinfo" style="background-size: cover; background-repeat: no-repeat;" 
			<?php if($dataInterchange):?> data-interchange="<?=$dataInterchange?>" <?php endif; ?>>
				<div class="inner-footer grid-x grid-container grid-x-padding">						
					<?php if ( $additionalFooter ):?>
					<div class="small-12 cell additional-footer">
						<?=$addtionalFooterContent;?>
					</div>	
					<?php endif;?>
<!--
					
					<div class="small-12 medium-12 large-12 cell">
						<nav role="navigation">
    						<?php joints_footer_links(); ?>
    					</nav>
    				</div>
-->
					
					<div class="small-12 medium-12 large-12 cell">
						<hr/>
					</div>
					<div class="text-center xmedium-text-left small-12 xmedium-6">
						<?=$contentLeft;?>	
					</div>
					<div class="text-center xmedium-text-right small-12 xmedium-6">
						<?=$contentRight;?>	
					</div>						
				</div> <!-- end #inner-footer -->
			</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		<?php get_template_part( 'parts/components/component', 'modals' );?>
		<?php wp_footer(); ?>		
	</body>
	
</html> <!-- end page -->