<?php 

add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {

    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

/**
 * Add styles/classes to the "Styles" drop-down
 */ 
add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );

function fb_mce_before_init( $settings ) {

    $style_formats = array(
         array(
            'title' => 'Buttons',
                'items' => array(
                array(
                    'title' => 'primary button',
					'selector' => 'a',
			   		'classes' => 'button'
                ),
                array(
                    'title' => 'secondary button',
					'selector' => 'a',
			   		'classes' => 'button secondary'
                ),
                array(
                    'title' => 'primary button expanded',
					'selector' => 'a',
			   		'classes' => 'button expanded'
                ),
                array(
                    'title' => 'secondary button expanded',
					'selector' => 'a',
			   		'classes' => 'button secondary expanded'
                )                
            )
        ),
        array(
            'title' => 'Expand-Collapse',
            'block' => 'div',
            'classes' => 'mobileExpand',
            'wrapper' => true
        ),
        array(
            'title' => 'Red Uppercase Text',
            'inline' => 'span',
            'styles' => array(
                'color'         => 'red', // or hex value #ff0000
                'fontWeight'    => 'bold',
                'textTransform' => 'uppercase'
            )
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}
