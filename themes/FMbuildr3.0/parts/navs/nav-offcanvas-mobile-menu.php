<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<?php 
	$mobileNavStyle = get_theme_mod("offcanvas_select_setting_id"); 
	$tabletNavStyle = get_theme_mod("offcanvas_select_setting_tablet_id"); 
	$mobileTransition = get_theme_mod("offcanvas_transition_id");
	$tabletTransition = get_theme_mod("offcanvas_transition_tablet_id");
	$logo = get_field('logo','options');
?> 


<div class="off-canvas position-left" data-mobile="<?php echo $mobileNavStyle." ".$mobileTransition ; ?>" data-tablet="<?php echo $tabletNavStyle." ".$tabletTransition; ?>" id="off-canvas" data-off-canvas data-transition="overlap">
	<?php 
		$phone = get_field('phone_number','option');
		$off_canvas_cta = get_field('off_canvas_cta','options');
		$off_canvas_cta_link = get_field('off_canvas_cta_link','options');
	?>
<div class="grid-y" style="height:100%">
		<div class="cell shrink">
			<div class="grid-x offcanvas-phone-wrap">
				<div class="cell shrink offcanvas-phone-container-2"><a data-toggle="off-canvas" class="hamburger control"><i class="fa fa-close" ></i></a></div>
				<div class="cell auto text-center offcanvas-phone-container">
						<img id="img-fixed" data-interchange="[<?=$logo['url'];?>, small], [<?=$logo['url'];?>, medium], [<?=$logo['url'];?>, large]" height="<?=$logo['height'];?>" width="<?=$logo['width'];?>" alt="<?=$logo['alt'];?>"/ >			
				</div>
				<div class="cell shrink text-right offcanvas-phone-container-2">
					<a href="tel:+1<?php echo _phone_num($phone);?>"><i class="fa fa-phone"></i></a>
				</div>
			</div>	
		</div>
		<div class="auto cell" style="overflow: auto">	
			<?php joints_topleft_nav(); ?>	
			<?php joints_off_canvas_nav(); ?>
			<div class="off-cv-wrapper">
				<?php echo do_shortcode('[offcanvasSocial]'); ?>
			</div>
		</div>
		<div class="cell shrink">			
			<div class="off-canvas-cta-button">
				<a href="<?=$off_canvas_cta_link?>" class="button primary expanded"><?=$off_canvas_cta?></a>
			</div>
		</div>
	</div>
</div>

