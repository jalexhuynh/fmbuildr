<?php	  

	
if( have_rows('flexible_section_fields') ):
  // loop through the rows of data
    while ( have_rows('flexible_section_fields') ) : the_row();

		if ( get_row_layout() == 'new_row'):
	
		$customID = get_sub_field( 'unique_id' );
		$customClass = get_sub_field('custom_classes');
		$add_bg_images = get_sub_field('add_bg_images');
		$bgImgs = get_sub_field('background_images');
		$fp_background_color = get_sub_field('fp_background_color');
		$add_column_padding = get_sub_field('add_column_padding');
		$fp_parallax_images = get_sub_field('fp_parallax_images');
		$data_attributes = get_sub_field('data_attributes');		
		$type_of_row = get_sub_field('type_of_row');
		$interchangeImgs = dataInterchangeImgs($bgImgs);	
		$type_of_parallax = get_sub_field('type_of_parallax');	 
 
		?>		
		<section id="<?=$customID;?>" class="fp-section-wrapper">			
			<div class="fp-sections <?=$customClass?> <?php if($fp_parallax_images): echo $type_of_parallax; endif;?>"  <?php if($add_bg_images): ?>data-interchange="<?=$interchangeImgs?>" style="background-color:<?=$fp_background_color?>" <?php endif; ?> <?=$data_attributes?> >			 
				<div class="grid-x <?=$type_of_row?> <?=$customClass;?> <?=$add_column_padding?>" >
				<?php if( have_rows('columns') ):?>		
					<?php while ( have_rows('columns') ) : the_row();?>			
						<?php 
							$colContent = get_sub_field('column_content');
							$add_content = get_sub_field('add_content');	
							$colClasses = get_sub_field('custom_classes');
							$smCol = get_sub_field('sm_size');
							$mdCol = get_sub_field('md_size');
							$xmdCol = get_sub_field('xmd_size');
							$lgCol = get_sub_field('lg_size');
							$xlgCol = get_sub_field('xlg_size');
							$add_slider = get_sub_field('add_slider');
							$column_data_attribute = get_sub_field('column_data_attribute');
							$background_image_cell = get_sub_field('background_image_cell');
							$add_background_image = get_sub_field('add_background_image');
							$interchangeImgInternal = dataInterchangeImgs($background_image_cell);	
						?>
						<div class="cell <?=$colClasses;?> small-<?=$smCol;?> medium-<?=$mdCol;?> xmedium-<?=$xmdCol;?> large-<?=$lgCol;?> xlarge-<?=$xlgCol?>"	<?=$column_data_attribute?> <?php if($background_image_cell && $add_background_image): ?> style="height:100%" <?php endif; ?>>
							<div class="cell-background" <?php if($background_image_cell && $add_background_image): ?> data-interchange="<?=$interchangeImgInternal?>" style="height:100%" <?php endif; ?> >
								<?php if($add_content): echo $colContent; endif; ?>
								<?php if(have_rows('slider') && $add_slider): $ctr = 0; ?>
									<div class="fp-carousel-row-inner">
										<?php while(have_rows('slider')): the_row(); 
											$title = get_sub_field('title');
											$content = get_sub_field('content'); ?>
											<div class="carousel-cell">
												<div class="fp-carousel-slide-container text-center"><?=$content; ?></div>
											</div>
										<?php endwhile; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>			
							
					<?php endwhile;?>						
				<?php endif;?>		
				</div>
			</div>		
		</section>	
		<?php endif;?>		
	<?php endwhile; ?>	
<?php else : ?>

no layouts found

<?php  // no layouts found
endif; ?>

  