<?php 
	$add_a_video_banner = get_field('add_a_video_banner');
	$type_of_banner = get_field('type_of_banner');
	$video_mp4 = get_field('video_mp4');
	$video_ogg = get_field('video_ogg');
	$video_webm = get_field('video_webm');
	$background_images = get_field('background_images');
	$overlay_color = get_field('overlay_color');
	$overlay_opacity = get_field('overlay_opacity');
	$type_of_parallax = get_field('type_of_parallax');
	$add_an_image_to_banner = get_field('add_an_image_to_banner');
	$banner_images = get_field('banner_images');
	$tagline = get_field('tagline');
	$min_height_desktop = get_field('min_height_desktop');
	$min_height_tablet_1024 = get_field('min_height_tablet_1024');
	$min_height_tablet_768 = get_field('min_height_tablet_768px');
	$min_height_mobile_640 = get_field('min_height_mobile_640');
	$min_height_mobile_320 = get_field('min_height_mobile_320');	
	
	
	
	if ($background_images):
	/* loop through the images in the banner background images gallery field and save them as an array 
	** for use in our data-interchange background src.  
	** dataInterchangeImgs() can be found on assets/functions/theme-support.php --- line 75
	*/
		$dataInterchange = dataInterchangeImgs($background_images);
	endif;
	

	 if($background_images): $ctr=0; $theimg = array(); 					
		foreach( $background_images as $image ): $ctr++; 							 
			$theImg[$ctr] = $image['url']; 
			$theAlt[$ctr] = $image['alt'];
			$theWidth[$ctr] = $image['width'];
			$theHeight[$ctr] = $image['height'];								
		endforeach;	
	endif; 	
	
	$parallaxType = "";
	if($type_of_parallax):		
			if($type_of_parallax == "none"):
				$parallaxType = "";
			elseif($type_of_parallax == "perspective"):
				$parallaxType = "banner-parallax";
			elseif($type_of_parallax == "fixed"):
				$parallaxType = "parallax-fixed";
			endif;		
	endif;

?>
<style>
	/*
	-----------------------------------------------------------------------------------------------------------------------------
	-- reversed the logic of and order of inline media queries for front-page banner height settings to a mobile first pattern --
	-----------------------------------------------------------------------------------------------------------------------------
	small: 0 and up,
	smedium: 420px and up,
	medium: 640px and up,
	xmedium: 769px and up,
	large: 1025px and up,
	xlarge: 1200px and up,
	xxlarge: 1440px and up,
	largest: 1600px and up,
	-----------------------------------------------------------------------------------------------------------------------------
	*/
	.banner-wrapper, #fp-frontpage-banner , .tagline { min-height:<?=$min_height_mobile_320?>px; }
	.banner-wrapper, #fp-frontpage-banner , .tagline p:last-of-type{margin-bottom: 0;}
	@media (min-width:420px) { .banner-wrapper, #fp-frontpage-banner , .tagline { min-height:<?=$min_height_mobile_640?>px; } }
	@media (min-width:640px) { .banner-wrapper, #fp-frontpage-banner , .tagline { min-height:<?=$min_height_tablet_768?>px; } }
	@media (min-width:769px) { .banner-wrapper, #fp-frontpage-banner , .tagline { min-height:<?=$min_height_tablet_1024?>px; } }
	@media (min-width:1025px) { .banner-wrapper, #fp-frontpage-banner , .tagline { min-height:<?=$min_height_desktop?>px; } }		
	
	<?php if($overlay_opacity != "" || $overlay_opacity != 0): ?>
	.banner-wrapper:before {
		content:"";
		position: absolute;			
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=<?=$overlay_opacity?>)";
		opacity:calc(<?=$overlay_opacity?> / 100);
		-moz-opacity:calc(<?=$overlay_opacity?> / 100);
		-khtml-opacity:calc(<?=$overlay_opacity?> / 100);
		filter: alpha(opacity=<?=$overlay_opacity?>);
		background-color:<?=$overlay_color?>;	
		height:100%;
		width:100%;
		top:0;
		left:0;
		right:0;
		bottom:0;
		margin:auto;
		z-index:2;		
	}
	
	
	.ie .banner-wrapper:before {
		zoom:1;
		display: none;
	}
	
	<?php endif; ?>
		
</style>

<?php if($add_a_video_banner): ?>
	<section class="fp-sections" id="fp-frontpage-banner">		
			<div class="static-banner-wrapper banner-wrapper <?=$parallaxType?>" data-speed="0.2" <?php if($dataInterchange):?> data-interchange="<?=$dataInterchange?>" <?php endif; ?> alt="<?=$thealt[1]?>">
				<?php if($type_of_banner == "video"): ?>
					<div class="video">
						<?php if ($video_mp4 || $video_ogg  || $video_webm):?>	
						<video id="fpVideo" width="100%" height="100%" preload="auto" loop autoplay muted>
							<?php if($video_mp4) { ?><source type="video/mp4" src="<?php echo $video_mp4;?>"><?php } ?>
							<?php if($video_webm) { ?><source type="video/webm" src="<?php echo $video_webm;?>"><?php } ?>
							<?php if($video_ogg) { ?><source type="video/ogg" src="<?php echo $video_ogg;?>"><?php } ?>			
						</video>
						<?php endif; ?>		
					</div>
					<?php endif; ?>			
			</div>
			<div class="grid-x grid-padding-x tagline align-middle" >
				<div class="cell"><?php if($tagline): echo $tagline; endif; ?></div>
			</div>	
	</section>
<?php endif; ?>

