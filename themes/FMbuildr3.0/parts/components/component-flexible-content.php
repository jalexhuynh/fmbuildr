<?php 
	// This can be found @ Admin > Appearance > Customize > Sub Pages > Choose Sub Page Template
	$template_type = get_theme_mod("fmbuildr_subpage_type"); 
	$container_attr = "";
	$cell_attr = ""; 
	
	switch ($template_type) {
	
	/****
		USE this variables when making a new flexible field
	****/
		
    case "full_width_grid_10":
    	$grid_attr ="align-center grid-padding-x"; 
        $cell_attr = "small-12 medium-12 large-10";  
        $container_attr = "grid-container";             
        break; 
    case "full_width_grid":
        $cell_attr = "small-12 medium-12 large-12";
        $grid_attr =  "grid-padding-x"; 
        $container_attr = "grid-container";       
        break;
    case "full_width_grid_nc":
        $cell_attr = "small-12 medium-12 large-12";
        $grid_attr =  "";
        $container_attr = "";       
		break;
	}

	
	
	
	if( have_rows('flexible_page_sections') ): $tag = 0; ?>
			
	<?php while ( have_rows('flexible_page_sections') ) : the_row(); $tag++;?>

        <?php if ( get_row_layout() == 'column_grid_images'):?>
			<?php 
			$smallUp  = get_sub_field('small_up');
			$mediumUp  = get_sub_field('medium_up');
			$largeUp  = get_sub_field('large_up');
			?>
			
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">					
					<div class="grid-x grid-padding-x grid-padding-y small-up-<?=$smallUp;?> xmedium-up-<?=$mediumUp;?> large-up-<?=$largeUp;?> grid-image-cols">	
						<?php 
						$colImgs  = get_sub_field('col_grid_images');
						
						if( $colImgs ): ?>
						   
						        <?php foreach( $colImgs as $colImg ): ?>
									<div class="cell">
		
						                <a href="<?php echo $colImg['url']; ?>" data-fancybox="gallery">
						                     <img src="<?php echo $colImg['url']; ?>" width="<?=$colImg['width']?>" height="<?=$colImg['height']?>" alt="<?php echo $$colImg['alt']; ?>" />
						                </a>
						                <p class="galleryCaption"><?php echo $colImg['caption']; ?></p>
		
									</div>
						        <?php endforeach; ?>
						   
						<?php endif; ?>				
					</div>
				</div>
			</div>
			
        <?php elseif ( get_row_layout() == 'three_column'):?>

			<?php 
			$colOne = get_sub_field('column_one');
			$colTwo = get_sub_field('column_two');
			$colThree = get_sub_field('column_three');
			?>

			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<div class="grid-x grid-padding-x entry-content">
						<div class="cell small-12 xmedium-4">
							<?=$colOne;?>
						</div>
						<div class="cell small-12 xmedium-4">
							<?=$colTwo;?>
						</div>
						<div class="cell small-12 xmedium-4">
							<?=$colThree;?>
						</div>												
					</div>
				</div>
			</div>	     
        
        <?php elseif( get_row_layout() == 'full_width_content' ):?>
			<?php 
			$fullWidthContent = get_sub_field('full_width_content_area');
			?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">				
					<div class="grid-x entry-content">
						<div class="cell small-12">
							<?=$fullWidthContent;?>
						</div>
					</div>
				</div>
			</div>		
			
		<?php elseif( get_row_layout() == 'full_width_content_text_only' ):?>
			<?php 
			$fullWidthContent = get_sub_field('full_width_content_area_txt');
			?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<div class="grid-x entry-content">
						<div class="cell small-12">
							<?=$fullWidthContent;?>
						</div>
					</div>
				</div>
			</div>	
			
        <?php  elseif( get_row_layout() == 'fifty_fifty_content' ): ?>
			<?php 
			$ffContentLeft = get_sub_field('fifty_fifty_content_left');
			$ffContentRight = get_sub_field('fifty_fifty_content_right');					
			?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
		        	<div class="grid-x grid-padding-x entry-content" >
						<div class="cell small-12 medium-6">
							<?=$ffContentLeft;?>
						</div>
						<div class="cell small-12 medium-6">
							<?=$ffContentRight;?>
						</div>
					</div>
				</div>
			</div>		
			
		<?php elseif (get_row_layout() == 'four_eight_content'): ?>
			<?php 
			$feContentLeft = get_sub_field('four_eight_content_left');
			$feContentRight = get_sub_field('four_eight_content_right');
			$four_eight_swap_for_mobile = get_sub_field('four_eight_swap_for_mobile');
			
			if($four_eight_swap_for_mobile):
			?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
		        	<div class="grid-x grid-padding-x entry-content" >
						<div class="cell small-12 xmedium-8 xmedium-order-2">
							<?=$feContentRight;?>
						</div>							
						<div class="cell small-12 xmedium-4 xmedium-order-1">
							<?=$feContentLeft;?>
						</div>
					</div>	
				</div>
			</div>			
			<?php else: ?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">				
		        	<div class="grid-x grid-padding-x entry-content">
						<div class="cell small-12 xmedium-4">
							<?=$feContentLeft;?>
						</div>
						<div class="cell small-12 xmedium-8">
							<?=$feContentRight;?>
						</div>
					</div>	
				</div>
			</div>				
			<?php endif; ?>
			
		<?php elseif (get_row_layout() == 'eight_four_content'): ?>
			<?php 
			$efContentLeft = get_sub_field('eight_four_content_left');
			$efContentRight = get_sub_field('eight_four_content_right');
			$eight_four_swap_for_mobile = get_sub_field('eight_four_swap_for_mobile');
			
			if($eight_four_swap_for_mobile):
			?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
		        	<div class="grid-x grid-padding-x entry-content">
						<div class="cell small-12 xmedium-4 xmedium-order-2">
							<?=$efContentRight;?>
						</div>			        	
						<div class="cell small-12 xmedium-8 xmedium-order-1">
							<?=$efContentLeft;?>
						</div>
					</div>	
				</div>
			</div>
			<?php else: ?>
 			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	       	
		        	<div class="grid-x grid-padding-x entry-content">
						<div class="cell small-12 xmedium-8">
							<?=$efContentLeft;?>
						</div>
						<div class="cell small-12 xmedium-4">
							<?=$efContentRight;?>
						</div>
					</div>	
				</div>
 			</div>					
			<?php endif; ?>	
		
		<?php elseif (get_row_layout() == 'accordion'):?>		
			<?php $accordionTitle = get_sub_field('accordion_title');?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<?php if ($accordionTitle):?>
						<h3><?=$accordionTitle;?></h3>
					<?php endif;?>
					<?php if( have_rows('items') ):?>
					<ul class="accordion" data-accordion>
						
						<?php $ic = 0; while ( have_rows('items') ) : the_row(); $ic++;?>
						<?php // set up items for accordions and set the first to is-active
						$itemTitle = get_sub_field('item_title');
						$itemContent = get_sub_field('item_content');
						?>
						  <li class="accordion-item <?php if ($ic==1){ echo 'is-active';}?>" data-accordion-item>
						    <a href="#" class="accordion-title"><?=$itemTitle;?></a>
						    <div class="accordion-content" data-tab-content>
						      <?=$itemContent;?>
						    </div>
						  </li>						
					
					    <?php endwhile;?>
					</ul>	
					<?php endif;?>
				</div>
			</div>
		<?php elseif (get_row_layout() == 'tabbed_content'):?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">			
					<div class="grid-x entry-content">					
				<div class="cell small-12">
					<?php $tabbedAreaTitle = get_sub_field('tab_content_title');?>
					<h2><?=$tabbedAreaTitle;?></h2>
					
					<?php if( have_rows('tabs') ):?>
						<ul class="tabs" data-tabs id="<?=$tabbedAreaTitle;?>-content">
					    <?php $tabCount = 0; while ( have_rows('tabs') ) : the_row(); $tabCount++;?>
							<?php $tabTitle = get_sub_field('tab');?>
							<li class="tabs-title <?php if ($tabCount == 1):?>is-active<?php endif;?>"><a id="<?='panel'.$tabCount.'-label';?>" href="#panel<?=$tabCount;?>"><?=$tabTitle;?></a></li>
					    <?php endwhile;?>
					    </ul>
					<?php endif;?>	  
					<?php if( have_rows('tabs') ):?>
					    <div class="tabs-content" data-tabs-content="<?=$tabbedAreaTitle;?>-content">
					    <?php $tabCount = 0; while ( have_rows('tabs') ) : the_row(); $tabCount++;?>
					    	<?php $tabContent = get_sub_field('tab_content');?>
					    	<div id="panel<?=$tabCount;?>" class="tabs-panel <?php if ($tabCount == 1):?>is-active<?php endif;?>"><?=$tabContent;?></div>
						    	
						<?php endwhile; ?>
						</div>					
					<?php endif;?>
				</div>
			</div>
				</div>
			</div>
				
		<?php elseif (get_row_layout() == 'image_slider'): $tag++; ?>
			<?php 
				$slideshow_title = get_sub_field('slideshow_title');
				$enable_arrows = get_sub_field('enable_arrows');
				$enable_dots = get_sub_field('enable_dots');					
		        $enable_captions = get_sub_field('enable_captions');
		        $cell_width = get_sub_field( 'cell_width' );
		        $draggable = get_sub_field('draggable');
		        $wrap_around = get_sub_field('wrap_around');
		        $adaptive_height = get_sub_field('adaptive_height');
		        
		        
		        $arrows  = ($enable_arrows ? "true" : "false"); // returns true
		        $dots  = ($enable_dots ? "true" : "false"); // returns true
		        $drag  = ($draggable ? "true" : "false"); // returns true
		        $adapt = ($adaptive_height ? "true" : "false"); // returns true
		        $wrap  = ($wrap_around ? "true" : "false"); // returns true
				$the_cell_width = ($cell_width == "specify" ? get_sub_field('specify_width'): $cell_width ); ?>
				
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">					
			       <?php if($slideshow_title): ?><h2><?php echo get_sub_field('slideshow_title');?></h2><?php endif; ?>				
					<?php  $images = get_sub_field('slides'); 						
						if( $images ): ?>
						<?php $slc = 0; ?>
						<div class="slider-<?=$tag?> sub-page-carousel" data-flickity='{ "cellAlign": "center", "contain": true , "pageDots": <?=$dots?>, "prevNextButtons": <?=$arrows?>, "lazyLoad": true , "draggable": <?=$drag?>, "wrapAround": <?=$wrap?>, "adaptiveHeight": <?=$adapt?> }'>
						        <?php foreach( $images as $image ): $slc++; ?>
						           	<div class="carousel-cell" style="width:<?=$the_cell_width?>">						               
						               <img src="<?php echo $image['url']; ?>" style="width:100%" alt="<?php echo $image['alt']; ?>" />
						               
						                <?php if($enable_captions):?>
						                	<?php if($image['caption']): ?>
												<div class="slide-caption animated fadeInUp text-center" id="slide-caption-<?=$randId?>"><?php echo $image['caption']; ?></div>
											<?php endif; ?>
						                 <?php endif; ?>
						           	</div>
						        <?php endforeach; ?>
							</div>
						<?php endif; ?>
				</div>
			</div>		
									
		<?php elseif (get_row_layout() == 'content_slider'): $tag++; ?>
						<?php 
				$slideshow_title = get_sub_field('slideshow_title');
				$enable_arrows = get_sub_field('enable_arrows');
				$enable_dots = get_sub_field('enable_dots');					
		        $enable_captions = get_sub_field('enable_captions');
		        $cell_width = get_sub_field( 'cell_width' );
		        $draggable = get_sub_field('draggable');
		        $wrap_around = get_sub_field('wrap_around');
		        $adaptive_height = get_sub_field('adaptive_height');
		        
		        
		        $arrows  = ($enable_arrows ? "true" : "false"); // returns true
		        $dots  = ($enable_dots ? "true" : "false"); // returns true
		        $drag  = ($draggable ? "true" : "false"); // returns true
		        $adapt = ($adaptive_height ? "true" : "false"); // returns true
		        $wrap  = ($wrap_around ? "true" : "false"); // returns true
				$the_cell_width = ($cell_width == "specify" ? get_sub_field('specify_width'): $cell_width ); ?>

			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
		
					<?php if($slideshow_title): ?><h2><?php echo get_sub_field('slideshow_title');?></h2><?php endif; ?>
	
					<?php if( have_rows('slides') ):?>
						<?php $randId = rand(1,999); ?>
						<div class="slider-<?=$randId?> sub-page-carousel" data-flickity='{ "cellAlign": "center", "contain": true , "pageDots": <?=$dots?>, "prevNextButtons": <?=$arrows?>, "lazyLoad": true , "draggable": <?=$drag?>, "wrapAround": <?=$wrap?>, "adaptiveHeight": <?=$adapt?> }'>
							<?php $slc = 0; while ( have_rows('slides') ) : the_row(); 
								$slideContent = get_sub_field('slide_content');	 ?>
								<div class="carousel-cell" style="width:<?=$the_cell_width?>">	
									<?=$slideContent;?>
								</div>
							<?php $slc++; endwhile;?>
						</div>		
					<?php endif;?>
				</div>
			</div>			
			
		<?php elseif(get_row_layout() == 'video_content'): ?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<?php if(have_rows('videos')): $ctr = 0; ?>
						<div class="grid-x grid-padding-x" data-equalizer data-equalize-on="medium">
						<?php while(have_rows('videos')): the_row(); $ctr++; ?>
							<?php 
								$title = get_sub_field('title');
								$link = get_sub_field('link');
								$image = get_sub_field('image');										
							?>
							<div class="cell small-12 xsmall-6 medium-6 large-4 float-left">
							  <div data-equalizer-watch>
								<div class="text-center" style="line-height: 1.2; padding-bottom:20px;">
									<a href="<?php echo $link?>" data-fancybox="videos">
									<p><img src="<?php echo $image['url']?>" height="<?php echo $image['height']?>" width="<?php echo $image['width']?>"></p>
									<span><?php echo $title; ?></span>
									</a>
								</div>
							  </div>
							</div>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>							
		
		<?php elseif (get_row_layout()== 'horizontal_divider'):?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<hr/>
				</div>
			</div>
		<?php elseif (get_row_layout()== 'contact_form_widget'):?>
			<?php $contact_form_widget = get_sub_field('contact_form_widget'); ?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">				
					<div class="grid-x entry-content contact-form-widget contact-form-widget-<?=$tag?>">
				<div class="cell small-12">
					<?php echo do_shortcode($contact_form_widget);?>
				</div>
			</div>	
				</div>
			</div>
		<?php elseif (get_row_layout()== 'widgets'):?>	
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">			
					<div class="flexible-widgets twocol-widget twocol-<?=$tag?>">
			<div class="grid-x entry-content">
				<?php if(have_rows('widgets')): $ctr = 0; ?>
					<?php while(have_rows('widgets')): the_row(); $ctr++; ?>
						<div class="cell small-12 medium-6 widgeter widgeter-<?=$ctr?>">
							<?php $add_widget = get_sub_field('add_widget'); ?>							
							
							<?php if($add_widget == "4") :								
								$relatedGallery = get_sub_field('related_gallery');
								$gal_featured_image = get_sub_field('gal_featured_image'); ?>
											
								
							    <?php if( $relatedGallery ): ?>
							    <div class="widgetBoxWrap grid-x align-middle widget-gallery text-center">
							    <div class="cell small-12 widgetBox wYesBg  custTextWidget widgetBg text-center ">
							 		
								    <?php foreach( $relatedGallery as $post): // variable must be called $post (IMPORTANT) ?>
								    <?php setup_postdata($post); ?>
								        <?php $altTitle = get_field('alt_title');   ?>							     
										
										<?php if( have_rows('patient_galleries') ): ?>
										<div class="grid-y">
											<?php while ( have_rows('patient_galleries') ) : the_row(); ?>
												<?php $showOnArchive  = get_sub_field('show_in_archive');?>						
													<?php if( $showOnArchive): ?>
																		
														<div class="cell auto"><a href="<?php echo get_permalink(); ?>"><img class="alignnone" src="<?=$gal_featured_image['url'];?>" 
														alt="<?=$gal_featured_image['alt'];?>" width="<?=$gal_featured_image['width'];?>" height="<?=$gal_featured_image['height'];?>"/></a></div><div class="cell shrink"><a href="<?php echo get_permalink(); ?> " class="button primary expanded">VIEW THE GALLERY </a></div>				
															
													<?php endif; ?>				
											<?php endwhile; ?>	
										</div>		
										<?php endif; ?>
									    <?php endforeach; ?>
								    
								    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							    </div>
							  </div>
							  <?php endif; ?>

								
							<?php else: ?>
								<?php echo do_shortcode($add_widget);?>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>	
			</div>
		</div>	
				</div>
			</div>
		<?php elseif (get_row_layout()== 'full_custom_widget'):
				$full_custom_widget = get_sub_field('custom_widget'); 
				$before_and_after_cta = get_sub_field('before_and_after_cta_name');
				$before_and_after_cta_link = get_sub_field('before_and_after_cta_link');
			?>

			<?php echo do_shortcode($full_custom_widget);					
				if($before_and_after_cta_link != "/" && $before_and_after_cta ):
			?>
				<script type="text/javascript">
					jQuery("#widget-banda-button a").attr("href","<?=$before_and_after_cta_link;?>");
					jQuery("#widget-banda-button a").html('<?=$before_and_after_cta?>')
				</script>
				<?php endif; ?>			
						
			
        <?php elseif( get_row_layout() == 'form' ):?>
			<?php 
			$fwForm = get_sub_field('full_width_form');
			$fbg = get_sub_field('add_background');
			$fbgc = get_sub_field('form_bg_color');
			$fbgi = get_sub_field('form_bg_image');
			$invertText = get_sub_field('invert_text');
			?>
			<?php if ($invertText == 1){
				$textClass = 'whiteText';
			} else {
				$textClass = '';
			}?>
			<div class="grid-x <?=$container_attr ?> <?=$grid_attr?>">
				<div class="cell <?=$cell_attr?>">	
					<div class="grid-x fwForm">
				<?php if ($fbg == 'image'):?>
					<div class="cell small-12 formWrap wBgImg <?=$textClass;?>" style="background-image: url('<?=$fbgi["url"];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; background-color: <?=$fbgc;?>;">
						<?=$fwForm;?>
					</div>
				<?php elseif($fbg == 'color'):?>
					<div class="cell small-12 formWrap wBgColor <?=$textClass;?>" style="background-color: <?=$fbgc;?>;">
						<?=$fwForm;?>
					</div>						
				<?php else:?>
					<?=$fwForm;?>
				<?php endif;?>
					
			</div>
				</div>
			</div>
        <?php endif;?>

    <?php endwhile;?>

<?php endif;?>