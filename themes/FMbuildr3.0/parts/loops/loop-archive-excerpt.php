<article id="post-<?php the_ID(); ?>" <?php post_class('animated fadeIn'); ?> role="article">					
	<header class="">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h3><?php the_title(); ?></h3></a>
		<?php /* get_template_part( 'parts/content', 'byline' ); */ ?>
	</header> <!-- end article header -->
	<?php
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	if (function_exists('vp_get_thumb_url')):
		// Set the desired image size. Swap out 'thumbnail' for 'medium', 'large', or custom size 
		$thumb=vp_get_thumb_url($post->post_content, 'medium'); 

	endif;
	?>				
	<section class="entry-content" itemprop="articleBody">
		<div class="grid-x grid-padding-x">
			<?php if($feat_image): ?>
			<div class="cell small-12 medium-5"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a></div>
			<div class="cell small-12 medium-7"><?php echo get_the_excerpt(); ?><br><a href="<?php the_permalink() ?>" class="tiny">Read more »</a>					
			<?php elseif ($thumb):?>			
			    <div class="cell small-12 medium-5">
				    <a href="<?php the_permalink();?>" >
					    <img class="alignnone" src="<?php echo $thumb; ?>"  />
					</a>
				</div>
				<div class="cell small-12 medium-7">			
					<?php echo get_the_excerpt(); ?>... <br><a href="<?php the_permalink() ?>" class="tiny">Read more »</a>
				</div>						
			<?php else:?>   			
				<div class="cell small-12 medium-12">
					<?php echo get_the_excerpt(); ?>...<br><a href="<?php the_permalink() ?>"class="tiny">Read more »</a>
				</div>	
			<?php endif;?>	
			
			</div>				
	</section> <!-- end article section -->

	<hr>			
	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->				    						
</article> <!-- end article -->
