<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>

<?php get_template_part('parts/components/component', 'banner');?>

<div class="content" id="content">
	
	<div class="grid-container">
	
		<div class="inner-content grid-x grid-padding-x align-center">	
	
			<main class="main small-12 medium-12 large-10 cell" role="main">

				<article class="content-not-found">
				
					<header class="article-header">
						<h2>
							<?php _e( 'Error 404 - Page Not Found', 'fmBuildr' ); ?>
						</h2>
					</header> <!-- end article header -->
			
					<section class="entry-content">
						<p><?php _e( 'The page or article you were looking for was not found, but maybe try looking again!', 'fmBuildr' ); ?></p>
					</section> <!-- end article section -->

					<section class="search">
					    <p><?php //get_search_form(); ?></p>
					</section> <!-- end search section -->
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

</div> <!-- end .grid-container -->

<?php get_footer(); ?>